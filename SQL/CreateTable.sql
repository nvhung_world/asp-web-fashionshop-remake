﻿-- Thông Tin Người Dùng ....................................................................
-- Người dùng
create table NguoiDung(
	ID_ND int not null  IDENTITY(1,1) primary Key,
	TaiKhoan varchar(30) not null UNIQUE,
	MatKhau varchar(30) not null,
	DangNhapCuoi date not null default getdate(),
	NgayLap date not null default getdate(),
	CHECK(LEN(MatKhau) >= 4)
);
insert into NguoiDung (TaiKhoan, MatKhau) values('admin', 'admin');
insert into NguoiDung (TaiKhoan, MatKhau) values('thuhai', 'thuhai');
-- Thông tin người dùng
create table NguoiDung_TT(
	ID_NDTT int not null primary key,
	Anh varchar(max),
	Ten nvarchar(10),
	Ho nvarchar(20),
	GioiTinh bit,
	NgaySinh date,
	CMT varchar(11),
	SDT varchar(11),
	Email nvarchar(50),
	DiaChi nvarchar(100),
    FOREIGN KEY (ID_NDTT) REFERENCES NguoiDung (ID_ND) ON DELETE CASCADE ON UPDATE CASCADE
);
insert into NguoiDung_TT (ID_NDTT) values(1);
insert into NguoiDung_TT (ID_NDTT) values(2);
-- Phân Quyền Truy Cập ....................................................................
-- Danh sách quyền truy cập
create table Quyen(
	ID_Q int not null  IDENTITY(1,1) primary Key,
	NoiDung nvarchar(20) not null UNIQUE
);
insert into Quyen (NoiDung) values(N'Admin');
insert into Quyen (NoiDung) values(N'Đã Khóa');
insert into Quyen (NoiDung) values(N'Khách Hàng');
-- Danh sách phân quyền
create table PhanQuyen(
	ID_PQ  int not null  IDENTITY(1,1) primary Key,
	ID_ND int not null,
	ID_Q int not null,
	FOREIGN KEY (ID_ND) REFERENCES NguoiDung (ID_ND) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (ID_Q) REFERENCES Quyen (ID_Q) ON DELETE CASCADE ON UPDATE CASCADE
);
insert into PhanQuyen (ID_ND, ID_Q) values(1, 1);
insert into PhanQuyen (ID_ND, ID_Q) values(1, 3);
insert into PhanQuyen (ID_ND, ID_Q) values(2, 3);
-- Nhập Kho ....................................................................
-- Nhà cung cấp
create table NhaCungCap(
	ID_NCC int not null IDENTITY(1,1) PRIMARY KEY,
	Ten nvarchar(30),
	SDT varchar(11),
	Email nvarchar(50),
	DiaChi nvarchar(100),
	NgayTao date not null default getdate()
);
insert into NhaCungCap(Ten) values (N'May 10');
insert into NhaCungCap(Ten) values (N'Adidas');
-- Thông Tin Mặt Hàng ....................................................................
-- Mặt hàng
create table MatHang(
	ID_MH int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	ID_NCC int not null,
	TenMH nvarchar(50) NOT NULL,
	GiaNhap int not null default 0 CHECK( LEN(GiaNhap) >= 0),
	TonKho int NOT NULL DEFAULT 0 CHECK( LEN(TonKho) >= 0),
	NgayTao date not null default getdate(),
	FOREIGN KEY (ID_NCC) REFERENCES NhaCungCap (ID_NCC) ON DELETE CASCADE ON UPDATE CASCADE
);
insert into MatHang(TenMH, ID_NCC, GiaNhap, TonKho) values(N'Quần Âu', 1, 100000, 1);
insert into MatHang(TenMH, ID_NCC, GiaNhap, TonKho) values(N'Sơ mi Trắng', 2, 200000, 1);
-- Thông tin mặt hàng
CREATE TABLE MatHang_TT (
	ID_MHTT int not null PRIMARY KEY,
	GiaBan int NOT NULL DEFAULT 0 CHECK( LEN(GiaBan) >= 0),
	DangBan bit not null default 0,
	ThuongHieu nvarchar(50),
	Loai nvarchar(30) DEFAULT N'Không Dõ',
	DoiTuong nvarchar(20) NOT NULL DEFAULT 'All',
	ChatLieu nvarchar(20),
	NamSX int,
	LuotXem int NOT NULL DEFAULT 0 CHECK( LEN(LuotXem) >= 0),
	LuotThich int NOT NULL DEFAULT 0 CHECK( LEN(LuotThich) >= 0),
	MoTa varchar(1000),
	FOREIGN KEY (ID_MHTT) REFERENCES MatHang (ID_MH) ON DELETE CASCADE ON UPDATE CASCADE
);
-- Thông tin đơn nhập hàng
create table DonNhapHang(
	ID_DNH int not null IDENTITY(1,1) PRIMARY KEY,
	ID_MH int not null,
	ID_NV int not null,
	SoLuong int not null default 0 CHECK( LEN(SoLuong) >= 0),
	NgayNhap date not null default getdate(),
	GhiChu nvarchar(100),
	FOREIGN KEY (ID_MH) REFERENCES MatHang (ID_MH) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (ID_NV) REFERENCES NguoiDung (ID_ND) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO DonNhapHang(ID_MH,ID_NV, SoLuong) VALUES (1,1,1)
INSERT INTO DonNhapHang(ID_MH,ID_NV,SoLuong) VALUES (2,1,1)
-- Danh sách ảnh mô tả mặt hàng
create table MatHang_Anh(
	ID_MHA int not null IDENTITY(1,1) PRIMARY KEY,
	ID_MH int not null,
	Anh varchar(max),
	Loai int not null default 0 CHECK( LEN(Loai) >= 0),
	FOREIGN KEY (ID_MH) REFERENCES MatHang (ID_MH) ON DELETE CASCADE ON UPDATE CASCADE
);
create table PhanHoi(
	ID_PH int not null IDENTITY(1,1) PRIMARY KEY,
	Email nvarchar(50),
	Ten nvarchar(30),
	NgayHoi datetime not null,
	NoiDung nvarchar(200) not null default '',
	NgayTraLoi datetime,
	TraLoi nvarchar(200)
);
-- Đơn Hàng ....................................................................
-- Đơn hàng
CREATE TABLE DonHang (
	ID_DH int not null IDENTITY(1,1) PRIMARY KEY,
	ID_ND int NOT NULL,
	TrangThai nvarchar(20) not null DEFAULT N'Giỏ Hàng',
	NgayTao date NOT NULL default getdate(),
	NgayThanhToan date,
	NoiGiao nvarchar(100),
	SDTNguoiNhan varchar(11),
	GhiChu nvarchar(100),
	FOREIGN KEY (ID_ND) REFERENCES NguoiDung (ID_ND) ON DELETE CASCADE ON UPDATE CASCADE
);
-- Thông tin đơn hàng
CREATE TABLE DonHang_CT (
	ID_DHCT int not null IDENTITY(1,1) PRIMARY KEY,
	ID_DH int NOT NULL,
	ID_MH int NOT NULL,
	SoLuong int NOT NULL DEFAULT 0 CHECK( LEN(SoLuong) >= 0),
	GiaBan int NOT NULL DEFAULT 0 CHECK( LEN(GiaBan) >= 0),
	FOREIGN KEY (ID_MH) REFERENCES MatHang (ID_MH) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (ID_DH) REFERENCES DonHang (ID_DH) ON DELETE CASCADE ON UPDATE CASCADE
);