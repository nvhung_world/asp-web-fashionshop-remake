﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form
{
    public partial class GioHang : System.Web.UI.Page
    {
        string userID = "",
            IDDH = "";
        bool isMin = false,
            isAdmin;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                Response.Redirect("TaiKhoan/Login.aspx");
                return;
            }
            else
            {
                SqlUser user = Session["User"] as SqlUser;
                userID = user.ID;
                isAdmin = user.isAdmin;
                isMin = true;
            }
            if(Request.QueryString["UserID"] != null && userID != Request.QueryString["UserID"] && isAdmin)
            {
                userID = Request.QueryString["UserID"];
                isMin = false;
            }
            if (Request.QueryString["IDDH"] != null)
                IDDH = Request.QueryString["IDDH"];
            
            if(!IsPostBack)
            {
                SqlAdapter adapter = new SqlAdapter();
                adapter.Connect();
                Load_KhachHang_TT();
                Load_DS(adapter);
                LoadDonHang(adapter);
                adapter.Close();
            }
        }

        private void Load_KhachHang_TT()
        {
            SqlUser user = null;
            if (!isMin)
            {
                try
                {
                    user = new SqlUser(userID);
                }
                catch (Exception) {
                }
            }
            else
                user = Session["User"] as SqlUser;

            if(user != null)
            {
                user.LoadInfo();
                txt_TTKH_Ten.Text = user.GetInfo("Ho", "") + " " + user.GetInfo("Ten", "");
                txt_TTKH_SDT.Text = user.GetInfo("SDT", "");
                txt_TTKH_Email.Text = user.GetInfo("Email", "");
                txt_TTKH_DC.Text = user.GetInfo("DiaChi", "");
                txt_TTKH_HyperLink.NavigateUrl = "~/Form/TaiKhoan/NguoiDung_TT.aspx?ID=" + user.ID; 
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi lấy thông tin người dùng');</script>");
        }

        private void Load_DS(SqlAdapter adapter, bool isNew = false)
        {
            string page = "0",
                whereName = "",
                order = "nhacungcap.ID_NCC ASC";

            if (!isNew && Request.QueryString["page"] != null)
                page = Request.QueryString["page"];
            if (page == "0")
                txtDS_Pre.Visible = false;

            if (txtDS_ID.Text != "")
                whereName = " and  DonHang.ID_DH = " + txtDS_ID.Text + " ";
            if(txtDS_TrangThai.SelectedIndex > 0)
            {
                whereName += " and " + txtDS_TrangThai.SelectedValue;
            }

            order = txtDS_SapXep.SelectedValue;

            string query = string.Format("SELECT DonHang.ID_DH, NgayTao, NgayThanhToan, TrangThai,  (case when TrangThai  like N'Giỏ Hàng' then SUM(MatHang_TT.GiaBan * DonHang_CT.SoLuong) else SUM(DonHang_CT.GiaBan * DonHang_CT.SoLuong) end)  as 'TongGia', sum(DonHang_CT.SoLuong) as 'So SP' FROM DonHang right join DonHang_CT on DonHang.ID_DH = DonHang_CT.ID_DH inner join MatHang_TT on MatHang_TT.ID_MHTT = DonHang_CT.ID_MH where DonHang.ID_ND = {0} {1} group by DonHang.ID_DH, NgayTao, NgayThanhToan, TrangThai order by  {2} OFFSET {3} ROWS FETCH NEXT 15 ROWS ONLY",
                userID,
                whereName,
                order,
                page);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow < 20)
                    txtDS_Nex.Visible = false;
                txtDS_Literal.Text = "";
                for (int i = 0; i < res.valueRow; i++)
                {
                    txtDS_Literal.Text += "<tr>";
                    txtDS_Literal.Text += "<td>" + res[i, 0].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + SqlUnit.ConvertSqlToDate(res[i, 1].ToString()) + "</td>";
                    txtDS_Literal.Text += "<td>" + SqlUnit.ConvertSqlToDate(res[i, 2].ToString()) + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 3].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 4].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 5].ToString() + "</td>";
                    string suaLink = string.Format("<a href = \"{0}\" class=\"btn btn-info\">Xem</a>",
                        GetLink(userID, res[i, 0].ToString(), ""));
                    txtDS_Literal.Text += string.Format("<td>{0}</td>",
                        suaLink);
                    txtDS_Literal.Text += "</tr>";
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể load danh sách');</script>");
        }
        protected void txtDS_Btn_Click(object sender, EventArgs e)
        {
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            Load_DS(adapter, true);
            adapter.Close();
        }
        protected void txtDS_Pre_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            if (page < 20)
                page = 0;
            else
                page -= 20;
            Response.Redirect(GetLink("", "", page.ToString()));
        }
        protected void txtDS_Nex_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            page += 20;
            Response.Redirect(GetLink("", "", page.ToString()));
        }

        protected void txtDH_Huy_Click(object sender, EventArgs e)
        {
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteNonTable("DELETE FROM DonHang WHERE TrangThai like N'Giỏ Hàng' and ID_DH = " + IDDH);
            if (res.isSuccess)
            {
                if (res.valueRow == 0)
                    Response.Write("<script>alert('Lỗi: Không thể xóa đơn hàng');</script>");
                else
                {
                    adapter.Close();
                    Response.Redirect("GioHang.aspx");
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể xóa đơn hàng');</script>");
            adapter.Close();
        }

        protected void txtDH_ThanhToan_Click(object sender, EventArgs e)
        {
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            string query = string.Format("SELECT ID_DHCT ,ID_MH ,SoLuong FROM DonHang_CT where ID_DH = " + IDDH);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                for(int i=0;i< res.valueRow; i++)
                {
                    string ID_DHCT = res[i, 0].ToString(),
                        ID_MH = res[i, 1].ToString();
                    int SoLuong = 0,
                        TonKho = 0,
                        GiaBan = 0;
                    int.TryParse(res[i, 2].ToString(), out SoLuong);
                    if (SoLuong <= 0)
                    {
                        DeleteDHCT(adapter, ID_DHCT);
                        continue;
                    }
                    SqlResult re = adapter.ExecuteTable("SELECT top 1 TonKho , GiaBan from MatHang , MatHang_TT where MatHang.ID_MH = MatHang_TT.ID_MHTT and MatHang.ID_MH = " + ID_MH);
                    if (res.isSuccess && res.valueRow > 0)
                    {
                        int.TryParse(re[0, 0].ToString(), out TonKho);
                        int.TryParse(re[0, 1].ToString(), out GiaBan);
                    }
                    else
                    {
                        DeleteDHCT(adapter, ID_DHCT);
                        continue;
                    }
                    if (SoLuong > TonKho)
                        SoLuong = TonKho;
                    string q = string.Format("UPDATE MatHang  SET TonKho -= {0} WHERE ID_MH = {1}; UPDATE DonHang_CT  SET SoLuong = {0}, GiaBan = {2} WHERE ID_DHCT = {3};",
                        SoLuong,
                        ID_MH,
                        GiaBan,
                        ID_DHCT);
                    re = adapter.ExecuteNonTable(q);
                    if(res.isSuccess && res.valueRow == 2)
                    {

                    }
                    else
                    {
                        DeleteDHCT(adapter ,ID_DHCT);
                        continue;
                    }
                }

                string w = string.Format("UPDATE DonHang SET TrangThai = N'Đang Chuyển', NoiGiao  = N'{0}', SDTNguoiNhan = '{1}', GhiChu = N'{2}' WHERE ID_DH = {3}",
                    txtDH_NoiGiao.Text,
                    txtDH_SdtNhan.Text,
                    txtDH_GhiChu.Text,
                    IDDH);
                SqlResult t = adapter.ExecuteNonTable(w);
                if(!t.isSuccess || t.valueRow <1)
                    Response.Write("<script>alert('Lỗi: Không thể xóa đơn hàng');</script>");
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể xóa đơn hàng');</script>");
            adapter.Close();
        }
        private bool DeleteDHCT(SqlAdapter adapter, string ID_DHCT)
        {
            SqlResult result = adapter.ExecuteNonTable("DELETE FROM DonHang_CT WHERE ID_DHCT = " + ID_DHCT);
            if (result.isSuccess)
                return true;
            else
                return false;
        }

        private string GetLink(string UserID = "", string IDDH = "", string page = "")
        {
            string _UserID = "",
                _IDDH = "",
                Page = "";

            if (UserID != "")
                _UserID = "UserID=" + UserID;
            else if (Request.QueryString["UserID"] != null)
                _UserID = "UserID=" + Request.QueryString["UserID"];

            if (IDDH != "")
                _IDDH = "IDDH=" + IDDH;
            else if (Request.QueryString["IDDH"] != null)
                _IDDH = "IDDH=" + Request.QueryString["IDDH"];

            if (page != "")
                Page = "page=" + page;
            else if (Request.QueryString["page"] != null)
                Page = "page=" + Request.QueryString["page"];

            return Request.Url.AbsolutePath + "?" + _UserID + (_UserID != "" && (_IDDH != "" || Page != "") ? "&" : "") + _IDDH + ((_IDDH != "" && _UserID != "") && Page != "" ? "&" : "") + Page;
        }
        private void LoadDonHang(SqlAdapter adapter)
        {
            if(IDDH == "")
            {
                panel.Visible = false;
                return;
            }
            else
                panel.Visible = true;

            string query = string.Format("SELECT DonHang.ID_DH, NgayTao, NgayThanhToan, TrangThai, (case when TrangThai  like N'Giỏ Hàng' then SUM(MatHang_TT.GiaBan * DonHang_CT.SoLuong) else SUM(DonHang_CT.GiaBan * DonHang_CT.SoLuong) end) as 'TongGia', sum(DonHang_CT.SoLuong) as 'So SP', NoiGiao, SDTNguoiNhan, GhiChu FROM DonHang right join DonHang_CT on DonHang.ID_DH = DonHang_CT.ID_DH inner join MatHang_TT on MatHang_TT.ID_MHTT = DonHang_CT.ID_MH where DonHang.ID_ND = {0}  and  DonHang.ID_DH = {1}  group by DonHang.ID_DH, NgayTao, NgayThanhToan, TrangThai, NoiGiao, SDTNguoiNhan, GhiChu ; select MatHang.ID_MH, SoLuong, (case when TrangThai  like N'Giỏ Hàng' then MatHang_TT.GiaBan * DonHang_CT.SoLuong else DonHang_CT.GiaBan * DonHang_CT.SoLuong end) as 'TongGia', TenMH, ThuongHieu, Loai  from DonHang inner join DonHang_CT on DonHang.ID_DH = DonHang_CT.ID_DH inner join MatHang on DonHang_CT.ID_MH = MatHang.ID_MH left join MatHang_TT on MatHang.ID_MH = MatHang_TT.ID_MHTT where DonHang.ID_DH = {1};",
                userID,
                IDDH);
            SqlResult res = adapter.ExecuteMultiTable(query); 
            if(res.isSuccess && res.TableCount == 2 && res.RowCount > 0 && res.TbRowCount[1] > 0)
            {
                //.............................................................................
                txtDH_ID.Text = res[0, 0].ToString();
                txtDH_NgayDat.Text = SqlUnit.ConvertSqlToDate(res[0, 1].ToString());
                txtDH_NgayThanhToan.Text = SqlUnit.ConvertSqlToDate(res[0, 1].ToString());
                txtDH_TinhTrang.Text = res[0, 3].ToString();
                txtDH_TongGiaTri.Text = res[0, 4].ToString();
                txtDH_SoHang.Text = res[0, 5].ToString();
                txtDH_NoiGiao.Text = res[0, 6].ToString();
                txtDH_SdtNhan.Text = res[0, 7].ToString();
                txtDH_GhiChu.Text = res[0, 8].ToString();
                if(txtDH_TinhTrang.Text == "Giỏ Hàng")
                {
                    txtDH_NoiGiao.Enabled = txtDH_SdtNhan.Enabled = txtDH_GhiChu.Enabled = true;
                    txtDH_Huy.Visible = txtDH_ThanhToan.Visible = true;
                }
                else
                {
                    txtDH_NoiGiao.Enabled = txtDH_SdtNhan.Enabled = txtDH_GhiChu.Enabled = false;
                    txtDH_Huy.Visible = txtDH_ThanhToan.Visible = false;
                }
                //.............................................................................
                txtDH_Literal.Text = "";
                for (int i=0; i < res.TbRowCount[1]; i++)
                {
                    txtDH_Literal.Text += "<tr>";
                    txtDH_Literal.Text += "<td>" + res[1, i, 0].ToString() + "</td>";
                    txtDH_Literal.Text += "<td>" + res[1, i, 3].ToString() + "</td>";
                    txtDH_Literal.Text += "<td>" + res[1, i, 2].ToString() + "</td>";
                    txtDH_Literal.Text += "<td>" + res[1, i, 1].ToString() + "</td>";
                    txtDH_Literal.Text += "<td>" + res[1, i, 4].ToString() + "</td>";
                    txtDH_Literal.Text += "<td>" + res[1, i, 5].ToString() + "</td>";
                    string suaLink = string.Format("<a href = \"MatHangTT.aspx?IDMH={0}\" class=\"btn btn-info\">Xem MH</a>",
                        res[1, i, 0].ToString());
                    txtDH_Literal.Text += "<td>" + suaLink + "</td>";
                    txtDH_Literal.Text += "</tr>";
                }
            }
            else
            {
                panel.Visible = false;
                Response.Write("<script>alert('Lỗi: Không thể load dữ liệu đơn hàng');</script>");
            }
        }
    }
}