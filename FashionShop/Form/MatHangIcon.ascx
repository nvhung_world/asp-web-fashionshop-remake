﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatHangIcon.ascx.cs" Inherits="FashionShop.Form.MatHangIcon" %>
<asp:Panel ID="Panel1" runat="server" CssClass="panel panel-default">
    <a href="MatHangTT.aspx?IDMH=<%=ID_MH%>" style="text-decoration:none">
        <div>
            <asp:Image ID="txtImage" runat="server" CssClass="img-rounded Fit" Width="100%" Height="200px" ImageAlign="Middle"/>
        </div>
        <div>
            <asp:Label ID="txtGiaBan" runat="server" Text=""></asp:Label>
        </div>
        <div>
            <asp:Label ID="txtTenMH" runat="server" Text=""></asp:Label>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <asp:Panel ID="txtPanel_Het" runat="server" >
                    <div style="color:red">
                        <i class="glyphicon glyphicon-remove"></i>Hết Hàng
                    </div>
                </asp:Panel>
                <asp:Panel ID="txtPanel_Con" runat="server" Visible="false">
                    <div style="color:green">
                        <i class="glyphicon glyphicon-ok"></i>Còn Hàng
                    </div>
                </asp:Panel>
            </div>
            <div class="col-sm-6">
                <asp:Label ID="txtThuongHieu" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </a>
</asp:Panel>
