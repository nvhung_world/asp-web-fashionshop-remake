﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Form/HomeMaster.Master" AutoEventWireup="true" CodeBehind="GioHang.aspx.cs" Inherits="FashionShop.Form.GioHang" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Bootstrap/css/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-sm-3">
            <div class="panel panel-warning">
                <div class="panel panel-heading">
                    Thông Tin Liên Hệ
                </div>
                <div class="panel panel-body">
                    <table class="table table-striped table-bordered" style="font-size:13px">
                        <thead>
                            <tr >
                                <th colspan="2" style="font-size:20px;color:dodgerblue">
                                    <asp:Label ID="txt_TTKH_Ten" runat="server" Text="Label"></asp:Label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>
                                    Email
                                </th>
                                <td>
                                    <asp:Label ID="txt_TTKH_Email" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Số ĐT
                                </th>
                                <td>
                                    <asp:Label ID="txt_TTKH_SDT" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    Địa Chỉ
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="txt_TTKH_DC" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div>
                        <asp:HyperLink ID="txt_TTKH_HyperLink" runat="server" NavigateUrl="~/Form/TaiKhoan/NguoiDung_TT.aspx">Thông Tin Tài Khoản</asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <asp:Panel ID="panel" runat="server">
                <div class="panel panel-warning">
                    <div class="panel panel-heading">
                        Thông Tin Đơn Hàng
                    </div>
                    <div class="panel panel-body">
                        <table class="table table-striped table-bordered" style="font-size:13px">
                            <thead>
                                <tr>
                                    <th>
                                        Mã Đơn Hàng
                                    </th>
                                    <th>
                                        Ngày Đặt
                                    </th>
                                    <th>
                                        Ngày Thanh Toán
                                    </th>
                                    <th>
                                        Tình Trạng
                                    </th>
                                    <th>
                                        Tổng Giá trị
                                    </th>
                                    <th>
                                        Số Mặt Hàng
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Label ID="txtDH_ID" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="txtDH_NgayDat" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="txtDH_NgayThanhToan" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="txtDH_TinhTrang" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="txtDH_TongGiaTri" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="txtDH_SoHang" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="width:70%; margin: 20px auto 10px auto">
                            <div class="row" style="margin-top:7px">
                                <div class="col-sm-2" style="padding-top:5px">
                                    Nơi Giao
                                </div>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtDH_NoiGiao" runat="server" CssClass="form-control"  MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:7px">
                                <div class="col-sm-2" style="padding-top:5px">
                                    Số ĐT Nhận
                                </div>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtDH_SdtNhan" runat="server" CssClass="form-control" TextMode="Number" MaxLength="11"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:7px">
                                <div class="col-sm-2" style="padding-top:5px">
                                    Ghi Chú
                                </div>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtDH_GhiChu" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control NoResize" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:7px">
                                <div class="col-sm-6">

                                </div>
                                <div class="col-sm-3" style="padding-right:0px">
                                    <asp:Button ID="txtDH_ThanhToan" runat="server" Text="Thanh Toán" Width="100%" CssClass="btn btn-primary" OnClick="txtDH_ThanhToan_Click"/>
                                </div>
                                <div class="col-sm-3">
                                    <asp:Button ID="txtDH_Huy" runat="server" Text="Hủy" Width="100%" CssClass="btn btn-danger" OnClick="txtDH_Huy_Click"/>
                                </div>
                            </div>
                        </div>
                        <div>
                            <table class="table table-hover" style="font-size:13px">
                                <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Tên Mặt Hàng
                                        </th>
                                        <th>
                                            Đơn Giá
                                        </th>
                                        <th>
                                            Số Lượng
                                        </th>
                                        <th>
                                            Thương Hiệu
                                        </th>
                                        <th>
                                            Loại
                                        </th>
                                        <th>

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Literal ID="txtDH_Literal" runat="server"></asp:Literal>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="panel panel-warning">
                <div class="panel panel-heading">
                    Danh Sách Đơn Hàng
                </div>
                <div class="panel panel-body">
                    <div class="row" style="width:100%; margin: auto auto auto auto">
                        <div class="col-sm-1" style="padding-top: 7px; text-align:right">
                            ID
                        </div>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtDS_ID" runat="server" CssClass="form-control" TextMode="Number" MaxLength="11"></asp:TextBox>
                        </div>
                        <div class="col-sm-3">
                            <asp:Button ID="txtDS_Btn" runat="server" Text="Tìm Kiếm"  CssClass="btn btn-info" OnClick="txtDS_Btn_Click"/>
                        </div>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="txtDS_TrangThai" runat="server"  CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="txtDS_Btn_Click">
                                <asp:ListItem Text="Tình Trạng: Tất Cả">
                                </asp:ListItem>
                                <asp:ListItem Text="Tình Trạng: Giỏ Hàng" Value=" TrangThai like N'Giỏ Hàng' ">
                                </asp:ListItem>
                                <asp:ListItem Text="Tình Trạng: Đang Chuyển Hàng" Value=" TrangThai like N'Đang Chuyển' ">
                                </asp:ListItem>
                                <asp:ListItem Text="Tình Trạng: Hoàn Tất" Value=" TrangThai like N'Hoàn Tất' ">
                                </asp:ListItem>
                                <asp:ListItem Text="Tình Trạng: Thất Bại" Value=" TrangThai like N'Thất Bại' ">
                                </asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <asp:DropDownList ID="txtDS_SapXep" runat="server"  CssClass="form-control"  AutoPostBack="true" OnSelectedIndexChanged="txtDS_Btn_Click">
                                <asp:ListItem Text="ID Tăng" Value=" DonHang.ID_DH asc "></asp:ListItem>
                                <asp:ListItem Text="ID Giảm"  Value=" DonHang.ID_DH desc "></asp:ListItem>
                                <asp:ListItem Text="Ngày Tăng" Value=" NgayTao asc, NgayThanhToan asc "></asp:ListItem>
                                <asp:ListItem Text="Ngày Giảm" Value=" NgayTao desc, NgayThanhToan desc "></asp:ListItem>
                                <asp:ListItem Text="Giá trị Tăng" Value=" 'TongGia' asc "></asp:ListItem>
                                <asp:ListItem Text="Giá Trị Giảm" Value=" 'TongGia' desc "></asp:ListItem>
                                <asp:ListItem Text="Số Lượng Tăng" Value=" 'So SP' asc "></asp:ListItem>
                                <asp:ListItem Text="Số Lượng Giảm" Value=" 'So SP' desc "></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="margin-top:10px;min-height:300px">
                        <table class="table table-hover" style="font-size:13px">
                            <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Ngày Tạo
                                    </th>
                                    <th>
                                        Ngày Thanh Toán
                                    </th>
                                    <th>
                                        Tình Trạng
                                    </th>
                                    <th>
                                        Tổng Giá Trị
                                    </th>
                                    <th>
                                        Số lượng
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Literal ID="txtDS_Literal" runat="server"></asp:Literal>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin: 20px auto 10px auto">
                        <div class="row">
                            <div class="col-sm-2">

                            </div>
                            <div class="col-sm-3">
                                <asp:Button ID="txtDS_Pre" runat="server" Font-Bold="true" Text="< Trước" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Pre_Click"/>
                            </div>
                            <div class="col-sm-2">

                            </div>
                            <div class="col-sm-3">
                                <asp:Button ID="txtDS_Nex" runat="server" Font-Bold="true" Text="Sau >" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Nex_Click"/>
                            </div>
                            <div class="col-sm-2">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
