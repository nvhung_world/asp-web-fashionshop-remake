﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Form/HomeMaster.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="FashionShop.Form.Home" %>

<%@ Register Src="~/Form/MatHangIcon.ascx" TagPrefix="uc1" TagName="MatHangIcon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>
        FashionShop
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-sm-3">
            <div class="panel panel-danger">
                <div class="panel panel-heading" style="padding-bottom: 5px">
                    Thương Hiệu
                </div>
                <div class="panel panel-body" style="padding-top: 0px; margin-bottom: 0px">
                    <div style="width:90%;margin: auto auto auto auto">
                        <asp:CheckBoxList ID="txtBL_ThuongHieu" runat="server" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel panel-heading" style="padding-bottom: 5px">
                    Đối Tượng
                </div>
                <div class="panel panel-body" style="padding-top: 0px; margin-bottom: 0px">
                    <div style="width:90%;margin: auto auto auto auto">
                        <asp:CheckBoxList ID="txtBL_DoiTuong" runat="server" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel panel-heading" style="padding-bottom: 5px">
                    Chất Liệu
                </div>
                <div class="panel panel-body" style="padding-top: 0px; margin-bottom: 0px">
                    <div style="width:90%;margin: auto auto auto auto">
                        <asp:CheckBoxList ID="txtBL_ChatLieu" runat="server" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel panel-heading" style="padding-bottom: 5px">
                    Loại Sử Dụng
                </div>
                <div class="panel panel-body" style="padding-top: 0px; margin-bottom: 0px">
                    <div style="width:90%;margin: auto auto auto auto">
                        <asp:CheckBoxList ID="txtBL_Loai" runat="server" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="panel panel-info">
                <div class="panel panel-heading" style="margin-bottom: 0px">
                    <div class="row">
                        <div class="col-sm-3" style="padding-top:7px">
                            Danh sách sản phẩm
                        </div>
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-2" style="text-align:right;padding-top:7px">
                            Sắp Xếp
                        </div>
                        <div class="col-sm-4">
                            <asp:DropDownList ID="txtDS_SapXep" runat="server" CssClass="form-control" AutoPostBack="true">
                                <asp:ListItem Text="Tên Tăng" Value=" TenMH asc "></asp:ListItem>
                                <asp:ListItem Text="Tên Giảm" Value=" TenMH desc "></asp:ListItem>
                                <asp:ListItem Text="Giá Tăng" Value=" GiaBan asc "></asp:ListItem>
                                <asp:ListItem Text="Giá Giảm" Value=" GiaBan desc "></asp:ListItem>
                                <asp:ListItem Text="Lượt Thích Tăng" Value=" LuotThich asc "></asp:ListItem>
                                <asp:ListItem Text="Lượt Thích Giảm" Value=" LuotThich desc "></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="panel panel-body" >
                    <div class="row" style="margin: 5px 5px 5px 0px">
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_1"/>
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_2" />
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_3" />
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_4" />
                        </div>
                    </div>
                    <div class="row" style="margin: 10px 5px 5px 0px">
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_5"/>
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_6" />
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_7" />
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_8" />
                        </div>
                    </div>
                    <div class="row" style="margin: 10px 5px 5px 0px">
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_9"/>
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_10" />
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_11" />
                        </div>
                        <div class="col-sm-3">
                            <uc1:MatHangIcon runat="server" ID="txtDS_MHIcon_12" />
                        </div>
                    </div>
                    <div style="margin: 15px auto auto auto;width:95%">
                        <div class="row">
                            <div class="col-sm-9">
                            </div>
                            <div class="col-sm-1" style="padding-top:5px">
                                Trang
                            </div>
                            <div class="col-sm-2">
                                <asp:DropDownList ID="txtDS_DropPage" runat="server" CssClass="form-control" AutoPostBack="true">
                                    <asp:ListItem Text="0" Value ="0"></asp:ListItem>
                                    <asp:ListItem Text="1" Value ="1"></asp:ListItem>
                                    <asp:ListItem Text="2" Value ="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value ="3"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
