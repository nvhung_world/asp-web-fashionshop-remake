﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="FashionShop.Form.TaiKhoan.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Đăng Ký</title>
    <script src="../../Bootstrap/js/bootstrap.js"></script>
    <link href="../../Bootstrap/css/bootstrap.css" rel="stylesheet" />
</head>
<body style="background-color: #808080">
    <form id="form1" runat="server">
        <div class="panel panel-danger" style="width: 30%; margin: 20px auto auto auto">
                <div class="panel panel-heading" style="text-align: center; font-size: 25px; font-weight: bolder">
                    Register
                </div>
                <div class="panel panel-body" style="margin-bottom: 0px">
                    <div>
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" ForeColor="Red" HeaderText="Lỗi!!!" ShowValidationErrors="True" />
                        <asp:Label ID="txtError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Họ
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtHo" runat="server" CssClass="form-control" MaxLength="20" TabIndex="1" ></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:RequiredFieldValidator  ID="RequiredFieldValidatorHo" runat="server" ErrorMessage="Họ không được bỏ trống" ControlToValidate="txtHo" Text="(*)"  ForeColor="Red"></asp:RequiredFieldValidator>                            
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Tên
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtTen" runat="server" CssClass="form-control" MaxLength="10" TabIndex="2" ></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTen" runat="server" ErrorMessage="Tên không được bỏ trống" ControlToValidate="txtTen" Text="(*)"  ForeColor="Red"></asp:RequiredFieldValidator>                         
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Giới Tính
                        </div>
                        <div class="col-sm-8">
                            <asp:CheckBox ID="GioiTinh_Nam" runat="server" Text="Nam" CssClass="checkbox-inline" Checked="true" TabIndex="3" OnCheckedChanged="GioiTinh_Nam_CheckedChanged" AutoPostBack="true"/>
                            <asp:CheckBox ID="GioiTinh_Nu" runat="server" Text="Nữ" CssClass="checkbox-inline" TabIndex="4" OnCheckedChanged="GioiTinh_Nu_CheckedChanged" AutoPostBack="true"/>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Email
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="30" TabIndex="5"></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Email sai định dạng" Text="(*)"  ForeColor="Red"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" ErrorMessage="Email không được bỏ trống" ControlToValidate="txtEmail" Text="(*)"  ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row"  style="margin-top: 10px">
                        <div class="col-sm-3">
                            Tài Khoản
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtAcc" runat="server" CssClass="form-control" MaxLength="30" TabIndex="6"></asp:TextBox>                            
                        </div>
                        <div class="col-sm-1">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorAcc" runat="server" ErrorMessage="Tài khoản tối thiểu 4 kí tự, tối đa 30 kí tự" ValidationExpression="^[\s\S]{4,30}$" ControlToValidate="txtAcc" Text="(*)" ForeColor="Red"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAcc" runat="server" ErrorMessage="Tài khoản không được bỏ trống" ControlToValidate="txtAcc" Text="(*)"  ForeColor="Red"></asp:RequiredFieldValidator>                         
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Mật Khẩu
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtPass" runat="server" TextMode="Password" CssClass="form-control" MaxLength="30" TabIndex="7"></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPass" runat="server" ErrorMessage="Mật khẩu tối thiểu 4 kí tự, tối đa 30 kí tự" ValidationExpression="^[\s\S]{4,30}$" ControlToValidate="txtPass" Text="(*)" ForeColor="Red"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" runat="server" ErrorMessage="Mật khẩu không được bỏ trống" ControlToValidate="txtPass" Text="(*)" ForeColor="Red"></asp:RequiredFieldValidator>                            
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Mật Khẩu
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtPass2" runat="server" TextMode="Password" CssClass="form-control" MaxLength="30" TabIndex="8"></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:CompareValidator ID="CompareValidatorPass2" runat="server" ErrorMessage="Xác nhận mật khẩu phải trùng khớp" ControlToValidate="txtPass2" ControlToCompare="txtPass" Text="(*)" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>
                    <div style="margin-top: 10px">
                        <asp:Button ID="btnDangKy" runat="server" Text="Đăng Ký" CssClass="btn btn-info" Width="100%" TabIndex="9" OnClick="btnDangKy_Click"/>
                    </div>
                </div>
                <div class="panel panel-footer" style="margin-bottom:0px; text-align: right">
                    FashionShop
                </div>
            </div>
    </form>
</body>
</html>
