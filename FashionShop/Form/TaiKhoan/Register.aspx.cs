﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form.TaiKhoan
{
    public partial class Register : System.Web.UI.Page
    {
        SqlAdapter adapte;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (adapte == null)
                adapte = new SqlAdapter();
        }
        protected string DangNhap(string Acc, string Pass)
        {
            string err = "";
            SqlUser user = null;
            try
            {
                user = new SqlUser(Acc, Pass);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }
            if (user != null)
                Session["User"] = user;
            return err;
        }
        protected void btnDangKy_Click(object sender, EventArgs e)
        {
            adapte.Connect();
            //..............................................
            string Acc = txtAcc.Text.Trim().ToLower(), 
                Pass = txtPass.Text.Trim(), 
                Pass2 = txtPass2.Text.Trim(), 
                Ho = txtHo.Text.Trim(), 
                Ten = txtTen.Text.Trim(), 
                GioiTinh = GioiTinh_Nam.Checked ? "0" : "1",
                Email = txtEmail.Text.Trim();
            if(Pass != Pass2)
            {
                txtError.Text = "Lỗi: Xác nhận mật khẩu không khớp!";
                return;
            }
            string query = string.Format("insert into NguoiDung (TaiKhoan, MatKhau) values(N'{0}', N'{1}');insert into phanquyen (ID_ND, id_Q) values(IDENT_CURRENT('NguoiDung'), 3);insert into NguoiDung_TT (ID_NDTT, Ho, ten, gioitinh, email) values(IDENT_CURRENT('NguoiDung'), N'{2}', N'{3}', {4}, N'{5}');",
                Acc,
                Pass,
                Ho,
                Ten,
                GioiTinh,
                Email);
            SqlResult res = adapte.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                if(DangNhap(Acc, Pass) == "")
                {
                    if (Session["oldURL"] == null)
                        Response.Redirect("../Home.aspx");
                    else
                        Response.Redirect(Session["oldURL"].ToString());
                }
                else
                    Response.Redirect("../Login.aspx");
            }
            else
            {
                txtError.Text = "Lỗi !!! Tên Tài khoản đã được đăng ký";
            }
            adapte.Close();
        }
        protected void GioiTinh_Nam_CheckedChanged(object sender, EventArgs e)
        {
            if (GioiTinh_Nam.Checked)
                GioiTinh_Nu.Checked = false;
            else
                GioiTinh_Nu.Checked = true;
        }
        protected void GioiTinh_Nu_CheckedChanged(object sender, EventArgs e)
        {
            if (GioiTinh_Nu.Checked)
                GioiTinh_Nam.Checked = false;
            else
                GioiTinh_Nam.Checked = true;
        }
    }
}