﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NguoiDung_TT.aspx.cs" Inherits="FashionShop.Form.TaiKhoan.NguoiDung_TT" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thông Tin Khách Hàng</title>
    <script src="../../Bootstrap/js/bootstrap.js"></script>
    <link href="../../Bootstrap/css/bootstrap.css" rel="stylesheet" />
    <style>
        .Fit{
            object-fit: contain;
        }
    </style>
    <script type="text/javascript">
        function showBrowseDialog() {
            var file = document.getElementById('<%=txtDDT_FileUpload.ClientID%>');
            file.click();
            file.onchange = function (e) {
                document.getElementById('txtDDT_img').src = URL.createObjectURL(e.target.files[0]);
            };
        }
    </script>
</head>
<body style="background-color: #808080">
    <form id="form1" runat="server">
        <div class="panel panel-success" style="width:60%; margin: 15px auto auto auto">
            <div class="panel panel-heading">
                Thông tin khách hàng
            </div>
            <div class="panel panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <asp:Image ID="txtTDTT_Img" ImageUrl="~/Image/Icon-user.png" runat="server" CssClass="img-thumbnail Fit" alt="" Width="100%" ImageAlign="Middle" Height="250px"/>
                    </div>
                    <div class="col-sm-8">
                        <asp:Panel ID="panel_TT" runat="server">
                            <div>
                                <asp:Label ID="lbError" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="row" style="margin-top:10px">
                                <div class="col-sm-3">
                                    Mã Tài Khoản
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbMaND" runat="server" Text="................" BackColor="#c0c0c0"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Ngày Lập
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbNL" runat="server" Text="................" BackColor="#c0c0c0"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Đăng Nhập Cuối
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbDNC" runat="server" Text="................" BackColor="#c0c0c0"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Họ Tên
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbHoTen" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Giới Tính
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbGioiTinh" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Ngày Sinh
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbNgaySinh" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Số CMT
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbCMT" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Số ĐT
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbSDT" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Email
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbEmail" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-3">
                                    Địa Chỉ
                                </div>
                                <div class="col-sm-9">
                                    <asp:Label ID="lbDiaChi" runat="server" Text="................"></asp:Label>
                                </div>
                            </div>
                            <div style="margin-top:5px;width:100%">
                                <asp:Button ID="lbDoiThongTin" runat="server" Width="100%" Text="Đổi Thông tin" CssClass="btn btn-primary" OnClick="lbDoiThongTin_Click"/>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panel_DTT" runat="server">
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-2">
                                    Họ
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtDDT_Ho" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Tên
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtDDT_Ten" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-2">
                                    Giới Tính
                                </div>
                                <div class="col-sm-4">
                                    <asp:CheckBox ID="txtDDT_GTNam" runat="server" Text="Nam" CssClass="checkbox-inline"/>
                                    <asp:CheckBox ID="txtDDT_GTNu" runat="server" Text="Nữ" CssClass="checkbox-inline"/>
                                </div>
                                <div class="col-sm-2">
                                    Ngày Sinh
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtDDT_NS" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-2">
                                    Số CMT
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtDDT_CMT" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    Số ĐT
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtDDT_SDT" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:5px">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            Email
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtDDT_Email" runat="server" TextMode="Email" CssClass="form-control" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:5px">
                                        <div class="col-sm-4">
                                            Địa Chỉ
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtDDT_DC" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            Ảnh Đại Diện
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:FileUpload ID="txtDDT_FileUpload" runat="server" CssClass="hidden"/>
                                            <img id="txtDDT_img" src="Icon-user.png" class="img-thumbnail Fit" style="width:100%; height:150px"  onclick="showBrowseDialog()"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top:5px;width:100%">
                                <div class="col-sm-6">
                                    <asp:Button ID="txtDTT_Update" runat="server" Width="100%" Text="Cập Nhật" CssClass="btn btn-primary" OnClick="txtDTT_Update_Click"/>
                                </div>
                                <div class="col-sm-6">
                                    <asp:Button ID="txtDTT_Huy" runat="server" Width="100%" Text="Hủy" CssClass="btn btn-danger" OnClick="txtDTT_Huy_Click"/>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
        <asp:Panel ID="panel_MK" runat="server" Width="100%">
            <div class="panel panel-success" style="width:60%; margin: 20px auto 20px auto">
                <div class="panel panel-heading">
                    Thay Đổi Mật Khẩu
                </div>
                <div class="panel panel-body">
                    <div class="row" style="margin-top:10px">
                        <div class="col-sm-3">
                            Mật Khẩu Hiện Tại
                        </div>
                        <div class="col-sm-9">
                            <asp:TextBox ID="txtTDMK_MKHT" runat="server" TextMode="Password" MaxLength="30" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-sm-3">
                            Mật Khẩu Mới
                        </div>
                        <div class="col-sm-9">
                            <asp:TextBox ID="txtTDMK_MKM" runat="server" TextMode="Password" MaxLength="30" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-sm-3">
                            Mật Khẩu Xác Nhận
                        </div>
                        <div class="col-sm-9">
                            <asp:TextBox ID="txtTDMK_MKM2" runat="server" TextMode="Password" MaxLength="30" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width:100%; margin-top:10px">
                        <div style="width:20%; margin: auto 0px auto auto">
                            <asp:Button ID="txtTDMK_btn" runat="server" Text="Đổi Mật Khẩu" CssClass="btn btn-primary" Width="100%" OnClick="txtDMK_Click"/>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
