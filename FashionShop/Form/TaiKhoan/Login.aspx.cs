﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form.TaiKhoan
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["User"] != null)
            {
                if (Session["oldURL"] == null)
                    Response.Redirect("../Home.aspx");
                else
                    Response.Redirect(Session["oldURL"].ToString());
            }
        }
        protected string DangNhap(string Acc, string Pass)
        {
            SqlUser user = null;
            string err = "";
            try
            {
                user = new SqlUser(Acc, Pass);
                if (user.isBlock)
                    err = "Lỗi: Tài khoản dã bị khóa";
                else
                    Session["User"] = user;
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }
            return err;
        }
        protected void btnDangNhap_Click(object sender, EventArgs e)
        {
            string Acc = txtAcc.Text.Trim(),
                Pass = txtPass.Text.Trim();
            string res = DangNhap(Acc, Pass);
            if (res == "")
            {
                if (Session["oldURL"] == null)
                    Response.Redirect("../Home.aspx");
                else
                    Response.Redirect(Session["oldURL"].ToString());
            }
            else
                txtError.Text = "Lỗi: " + res;
        }
    }
}