﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FashionShop.Form.TaiKhoan.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Đăng Nhập</title>
        <script src="../../Bootstrap/js/bootstrap.js"></script>
        <link href="../../Bootstrap/css/bootstrap.css" rel="stylesheet" />
    </head>
    <body style="background-color: #808080">
        <form id="form1" runat="server">
            <div class="panel panel-danger" style="width: 30%; margin: 20px auto auto auto">
                <div class="panel panel-heading" style="text-align: center; font-size: 25px; font-weight: bolder">
                    Login
                </div>
                <div class="panel panel-body" style="margin-bottom: 0px">
                    <div>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Lỗi !!!" ForeColor="Red"/>
                        <asp:Label ID="txtError" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Tài Khoản
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtAcc" runat="server" CssClass="form-control" MaxLength="30" TabIndex="1"></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAcc" runat="server" ErrorMessage="Tài khoản không được bỏ trống" Text="(*)" ForeColor="Red" ControlToValidate="txtAcc"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-3">
                            Mật Khẩu
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtPass" runat="server" TextMode="Password" CssClass="form-control" MaxLength="30" TabIndex="2"></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" runat="server" ErrorMessage="Mật khẩu không được bỏ trống" Text="(*)" ForeColor="Red" ControlToValidate="txtPass"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="margin-top: 10px">
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="Nhớ Tài Khoản" CssClass="checkbox" TabIndex="3"/>
                    </div>
                    <div>
                        <asp:Button ID="btnDangNhap" runat="server" Text="Đăng Nhập" CssClass="btn btn-primary" Width="100%" TabIndex="4" OnClick="btnDangNhap_Click"/>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-sm-6">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Form/TaiKhoan/QuenMatKhau.aspx">Quên mật khẩu</asp:HyperLink>
                        </div>
                        <div class="col-sm-6">
                            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Form/TaiKhoan/Register.aspx">
                                <div class="btn btn-info btn-sm" style="width:100%">
                                    Đăng Ký
                                </div>
                            </asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div class="panel panel-footer" style="margin-bottom:0px; text-align: right">
                    FashionShop
                </div>
            </div>
        </form>
    </body>
</html>
