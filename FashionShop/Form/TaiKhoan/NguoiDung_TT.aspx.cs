﻿using System;
using System.Data.SqlClient;
using FashionShop.SQL;
using System.IO;
namespace FashionShop.Form.TaiKhoan
{
    public partial class NguoiDung_TT : System.Web.UI.Page
    {
        SqlUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }

            string ID = null;
            if(Request.QueryString["ID"] != null)
                ID = Request.QueryString["ID"];
            else
                ID = (Session["User"] as SqlUser).ID;
            user = LoadUser(ID);
            if (!IsPostBack)
            {
                user.LoadInfo();
                LoadInfo();
                panel_TT.Visible = true;
                panel_DTT.Visible = false;
            }
        }
        private SqlUser LoadUser(string ID)
        {
            SqlUser user = null;
            if (ID == (Session["User"] as SqlUser).ID || (Session["User"] as SqlUser).isOnlyKhachHang)
            {
                user = Session["User"] as SqlUser;
                panel_MK.Visible = true;
                lbDoiThongTin.Visible = true;
            }
            else
            {
                panel_MK.Visible = false;
                lbDoiThongTin.Visible = false;
                try
                {
                    user = new SqlUser(ID);
                }
                catch (Exception)
                {
                    Response.Write("<script>alert('Lỗi: Thất bại khi  load dữ liệu');</script>");
                }
            }
            return user;
        }
        private void LoadInfo()
        {            
            if (user == null)
                return;
            lbMaND.Text = user.ID;
            lbNL.Text = SqlUnit.ConvertSqlToDate(user.GetInfo("NgayLap"));
            lbDNC.Text = SqlUnit.ConvertSqlToDate(user.GetInfo("DangNhapCuoi"));
            lbHoTen.Text = user.GetInfo("Ho", "................") + " " + user.GetInfo("Ten", "................");
            lbGioiTinh.Text = user.GetInfo("GioiTinh", "................");
            lbNgaySinh.Text = SqlUnit.ConvertSqlToDate(user.GetInfo("NgaySinh"));
            lbCMT.Text = user.GetInfo("CMT", "................");
            lbSDT.Text = user.GetInfo("SDT", "................");
            lbEmail.Text = user.GetInfo("Email", "................");
            lbDiaChi.Text = user.GetInfo("DiaChi", "................");

            string ImageUrl = SqlUnit.convertStringToImageUrl(user.GetInfo("Anh", ""));
            if (ImageUrl != "")
                txtTDTT_Img.ImageUrl = ImageUrl;
            else
                txtTDTT_Img.ImageUrl = "~/Image/Icon-user.png";
        }

        protected void lbDoiThongTin_Click(object sender, EventArgs e)
        {
            panel_TT.Visible = false;
            panel_DTT.Visible = true;
            if(lbHoTen.Text != null && lbHoTen.Text != "")
            {
                txtDDT_Ho.Text = lbHoTen.Text.Substring(0, lbHoTen.Text.LastIndexOf(" "));
                txtDDT_Ten.Text = lbHoTen.Text.Substring(lbHoTen.Text.LastIndexOf(" ") + 1);
            }
            else
            {
                txtDDT_Ho.Text = txtDDT_Ho.Text = "";
            }
            if(lbGioiTinh.Text == "Nữ")
            {
                txtDDT_GTNu.Checked = true;
                txtDDT_GTNam.Checked = false;
            }
            else
            {
                txtDDT_GTNu.Checked = false;
                txtDDT_GTNam.Checked = true;
            }
            txtDDT_NS.Text = lbNgaySinh.Text;
            txtDDT_CMT.Text = lbCMT.Text;
            txtDDT_SDT.Text = lbSDT.Text;
            txtDDT_Email.Text = lbEmail.Text;
            txtDDT_DC.Text = lbDiaChi.Text;
        }
        protected void txtDTT_Huy_Click(object sender, EventArgs e)
        {
            panel_TT.Visible = true;
            panel_DTT.Visible = false;
        }
        protected void txtDTT_Update_Click(object sender, EventArgs e)
        {
            string Ten = txtDDT_Ten.Text,
                Ho = txtDDT_Ho.Text,
                GioiTinh = txtDDT_GTNam.Checked ? "0" : "1",
                CMT = txtDDT_CMT.Text,
                SDT = txtDDT_SDT.Text,
                Email = txtDDT_Email.Text,
                diaChi = txtDDT_DC.Text,
                NgaySinh = SqlUnit.ConvertDateToSql(txtDDT_NS.Text);
            if (NgaySinh == "")
            {
                Response.Write("<script>alert('Lỗi: Ngày sinh không hợp lệ');</script>");
                return;
            }
            string err = user.UpdateInfo(GetImageString(), Ten, Ho, GioiTinh, NgaySinh,
                CMT, SDT, Email, diaChi);
            if(err == "")
            {
                user.LoadInfo();
                LoadInfo();
                panel_TT.Visible = true;
                panel_DTT.Visible = false;
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi update' );</script>");
        }
        protected string GetImageString()
        {
            string res = "";
            if (txtDDT_FileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(txtDDT_FileUpload.PostedFile.FileName);
                    if (SqlUnit.CheckExtensionImage(Path.GetExtension(filename)))
                        res = SqlUnit.ConvertImageToString(txtDDT_FileUpload.PostedFile.InputStream);
                    else
                        Response.Write("<script>alert('Lỗi: Sai định dạng ảnh jag, png, bmp, gif');</script>");
                }
                catch (Exception)
                {
                    Response.Write("<script>alert('Lỗi: Upload ảnh thất bại');</script>");
                }
            }
            return res;
        }
        protected void txtDMK_Click(object sender, EventArgs e)
        {
            string oldPass = txtTDMK_MKHT.Text.Trim(),
                newPass = txtTDMK_MKM.Text.Trim(),
                newPass2 = txtTDMK_MKM2.Text.Trim();
            if (newPass != newPass2)
            {
                Response.Write("<script>alert('Lỗi: Xác nhận mật khẩu mới sai');</script>");
                return;
            }
            if (oldPass == "" || newPass == "" || newPass2 == "")
            {
                Response.Write("<script>alert('Lỗi: Tất cả các trường không được bỏ trống');</script>");
                return;
            }
            string error = (Session["User"] as SqlUser).ChangerPass(oldPass, newPass);
            if (error != "")
                Response.Write("<script>alert('Lỗi: không xác định');</script>");
        }

    }
}