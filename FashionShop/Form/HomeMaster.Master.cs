﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FashionShop.Form
{
    public partial class HomeMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;

            if (Session["User"] == null)
            {
                panel_Home_Login.Visible = false;
                panel_Home_Logout.Visible = true;
                txtAdminHyperLink.Visible = false;
            }
            else
            {
                panel_Home_Login.Visible = true;
                panel_Home_Logout.Visible = false;
                if ((Session["User"] as SQL.SqlUser).isAdmin)
                    txtAdminHyperLink.Visible = true;
                else
                    txtAdminHyperLink.Visible = false;
            }
        }
    }
}