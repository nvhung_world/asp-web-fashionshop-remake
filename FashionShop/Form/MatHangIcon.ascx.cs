﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FashionShop.Form
{
    public partial class MatHangIcon : System.Web.UI.UserControl
    {
        public string ID_MH = "",
            ImageUrl = "",
            GiaBan = "",
            TenMH = "",
            ThuongHieu = "";
        public bool isOK = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtImage.ImageUrl = ImageUrl;
            txtGiaBan.Text = GiaBan;
            txtTenMH.Text = TenMH;
            txtThuongHieu.Text = ThuongHieu;
            txtPanel_Con.Visible = isOK;
            txtPanel_Het.Visible = !isOK;
        }
    }
}