﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
using System.IO;
namespace FashionShop.Form.Admin
{
    public partial class QL_MatHangTT : System.Web.UI.Page
    {
        string ID_MH = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                Response.Redirect("~/Form/TaiKhoan/Login.aspx");
                return;
            }
            SqlUser user = Session["User"] as SqlUser;
            if (!user.isAdmin)
                Response.Redirect("~/Form/Home.aspx");

            if (Request.QueryString["ID"] != null)
                ID_MH = Request.QueryString["ID"];
            if (!IsPostBack && Load_MH())
                panelTT.Visible = true;
            else
                panelTT.Visible = false;

            if (!IsPostBack)
                Load_DS();
        }

        private void Load_DS(bool isNew = false)
        {
            string page = "0",
                whereName = "",
                order = "ID_MHTT ASC";
            if (!isNew &&Request.QueryString["page"] != null)
                page = Request.QueryString["page"];
            if (page == "0")
                txtDS_Pre.Visible = false;
            if (txtDS_Name.Text != "")
                whereName = "where TenMH like N'%" + txtDS_Name.Text + "%'";
            order = txtDS_DropDownList.SelectedValue;

            string query = string.Format("SELECT ID_MH, TenMH, TonKho, GiaBan, DangBan,ThuongHieu,LuotXem,LuotThich,NamSX,DoiTuong,Loai,ChatLieu FROM MatHang_TT right join MatHang on MatHang.ID_MH = MatHang_TT.ID_MHTT {0} ORDER BY {1} OFFSET {2} ROWS FETCH NEXT 20 ROWS ONLY",
                whereName,
                order,
                page);
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {

                if (res.valueRow < 20)
                    txtDS_Nex.Visible = false;
                txtDS_Literal.Text = "";
                for (int i = 0; i < res.valueRow; i++)
                {
                    txtDS_Literal.Text += "<tr>";
                    txtDS_Literal.Text += "<td>" + res[i, 0].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 1].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 2].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 3].ToString() + "</td>";
                    string DangBan = res[i, 4].ToString() != "True" ? "Không Bán" : "Đang Bán"; 
                    txtDS_Literal.Text += "<td>" + DangBan + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 5].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 6].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 7].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 8].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 9].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 10].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 11].ToString() + "</td>";
                    string xemLink = string.Format("<a href = \"{0}\" class=\"btn btn-info\">Xem</a>",
                        GetLink(res[i, 0].ToString(), ""));
                    txtDS_Literal.Text += string.Format("<td>{0}</td>",
                        xemLink);
                    txtDS_Literal.Text += "</tr>";
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể load danh sách');</script>");
            adapter.Close();
        }
        protected void txtDS_Btn_Click(object sender, EventArgs e)
        {
            Load_DS(true);
        }
        protected void txtDS_Pre_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            if (page < 20)
                page = 0;
            else
                page -= 20;
            Response.Redirect(GetLink( "", page.ToString()));
        }
        protected void txtDS_Nex_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            page += 20;
            Response.Redirect(GetLink( "", page.ToString()));
        }
        private string GetLink(string id = "", string page = "")
        {
            string ID = "",
                Page = "";
            if (id != "")
                ID = "id=" + id;
            else if (Request.QueryString["id"] != null)
                ID = "id=" + Request.QueryString["id"];
            if (page != "")
                Page = "page=" + page;
            else if (Request.QueryString["page"] != null)
                Page = "page=" + Request.QueryString["page"];

            return Request.Url.AbsolutePath + "?" + ID + (ID != "" && Page != "" ? "&" : "") + Page;
        }

        private bool Load_MH()
        {
            if (ID_MH == "")
                return false;
            bool var = false;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            string query = string.Format("SELECT ID_MH, NgayTao, TenMH, TonKho, LuotXem, LuotThich, GiaBan, DangBan,ThuongHieu, NamSX, DoiTuong, Loai, ChatLieu, MoTa FROM MatHang_TT right join MatHang on MatHang.ID_MH = MatHang_TT.ID_MHTT Where ID_MH = {0}",
                ID_MH);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow > 0)
                {
                    txtTT_ID.Text = res[0, 0].ToString();
                    txtTT_NT.Text = SqlUnit.ConvertSqlToDate(res[0, 1].ToString());
                    txtTT_TenMH.Text = res[0, 2].ToString();
                    txtTT_TonKho.Text = res[0, 3].ToString();
                    txtTT_LuotXem.Text = res[0, 4] == DBNull.Value ? "0": res[0, 4].ToString();
                    txtTT_LuotThich.Text = res[0, 5] == DBNull.Value ? "0" : res[0, 5].ToString();
                    txtTT_GiaBan.Text = res[0, 6].ToString();
                    txtTT_DangBan.Checked = res[0, 7].ToString()  != "True" ? false : true;
                    txtTT_ThuongHieu.Text = res[0, 8].ToString();
                    txtTT_NanSX.Text = res[0, 9].ToString();
                    txtTT_DoiTuong.Text = res[0, 10].ToString();
                    txtTT_Loai.Text = res[0, 11].ToString();
                    txtTT_ChatLieu.Text = res[0, 12].ToString();
                    txtTT_MoTa.Text = res[0, 13].ToString();
                    Load_MH_Anh(adapter);
                    var = true;
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể load dữ liệu');</script>");
            adapter.Close();
            return var;
        }
        private void Load_MH_Anh(SqlAdapter adapter)
        {
            Image[] img = new Image[4];
            img[0] = Image1;
            img[1] = Image2;
            img[2] = Image3;
            img[3] = Image4;
            string query = string.Format("SELECT top 4 ID_MHA, Anh, Loai FROM MatHang_Anh where ID_MH = {0} order by loai asc",
                ID_MH);
            SqlResult res = adapter.ExecuteTable(query);
            if(res.isSuccess)
            {
                for(int i =0;i<res.valueRow;i++)
                {
                    string ID_MHA = res[i, 0].ToString();
                    img[i].Attributes.Add("ID_MHA", ID_MHA);
                    string url = SqlUnit.convertStringToImageUrl(res[i, 1].ToString());
                    if (url != "")
                        img[i].ImageUrl = url;
                    else
                        img[i].ImageUrl = "~/Image/NoImage-icon.png";
                }
            }
        }
        protected void txtTT_btn_Click(object sender, EventArgs e)
        {
            string GiaBan = txtTT_GiaBan.Text == "" ? "0" : txtTT_GiaBan.Text,
                DangBan = txtTT_DangBan.Checked ? "1" : "0",
                ThuongHieu = txtTT_ThuongHieu.Text,
                Loai = txtTT_Loai.Text,
                DoiTuong = txtTT_DoiTuong.Text,
                ChatLieu = txtTT_ChatLieu.Text,
                NamSx = txtTT_NanSX.Text == "" ? "0" : txtTT_NanSX.Text,
                MoTa = txtTT_MoTa.Text;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            string query = string.Format("UPDATE MatHang_TT SET GiaBan = {0}, DangBan = {1}, ThuongHieu = N'{2}', Loai = N'{3}', DoiTuong = N'{4}', ChatLieu = N'{5}', NamSX = {6}, MoTa = N'{7}' WHERE ID_MHTT = {8}",
                GiaBan,
                DangBan,
                ThuongHieu,
                Loai,
                DoiTuong,
                ChatLieu,
                NamSx,
                MoTa,
                ID_MH);
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow > 0)
                {
                    query = "";
                    UpdateImage(adapter);
                    adapter.Close();
                    Response.Redirect(Request.Url.PathAndQuery);

                }
                else
                    query = string.Format("INSERT INTO MatHang_TT( ID_MHTT, GiaBan, DangBan, ThuongHieu, Loai, DoiTuong, ChatLieu, NamSX, MoTa) VALUES( {8},{0} ,{1} , N'{2}', N'{3}',N'{4}', N'{5}',{6} , N'{7}' )",
                                GiaBan,
                                DangBan,
                                ThuongHieu,
                                Loai,
                                DoiTuong,
                                ChatLieu,
                                NamSx,
                                MoTa,
                                ID_MH);
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể cập nhật dữ liệu');</script>");
            if(query != "")
            {
                res = adapter.ExecuteNonTable(query);
                if(res.isSuccess)
                {
                    UpdateImage(adapter);
                    adapter.Close();
                    Response.Redirect(Request.Url.PathAndQuery);
                }
                else
                    Response.Write("<script>alert('Lỗi: Không thể cập nhật dữ liệu');</script>");
            }
            adapter.Close();
        }
        private void UpdateImage(SqlAdapter adapter)
        {
            FileUpload[] files = new FileUpload[4];
            files[0] = FileUpload1;
            files[1] = FileUpload2;
            files[2] = FileUpload3;
            files[3] = FileUpload4;
            Image[] images = new Image[4];
            images[0] = Image1;
            images[1] = Image2;
            images[2] = Image3;
            images[3] = Image4;
            string[] imageString = new string[] { "", "", "", "" };
            for(int i=0;i<4;i++)
            {
                if (files[i].HasFile == false)
                    continue;
                try
                {
                    string filename = Path.GetFileName(files[i].PostedFile.FileName);
                    if (SqlUnit.CheckExtensionImage(Path.GetExtension(filename)))
                        imageString[i] = SqlUnit.ConvertImageToString(files[i].PostedFile.InputStream);
                    else
                        Response.Write("<script>alert('Lỗi: Sai định dạng ảnh " + i +" jag, png, bmp, gif');</script>");
                }
                catch (Exception)
                {
                    Response.Write("<script>alert('Lỗi: Upload ảnh  " + i + " thất bại');</script>");
                }
            }
            for(int i =0;i<4; i++)
            {
                if (imageString[i] == "")
                    continue;
                string query = "";
                string ID_MHA = images[i].Attributes["ID_MHA"];
                if (ID_MHA == "" || ID_MHA == null)
                    query = string.Format("INSERT INTO MatHang_Anh (ID_MH, Anh , Loai) VALUES({0}, '{1}' , {2})",
                        ID_MH,
                        imageString[i],
                        i);
                else
                    query = string.Format("UPDATE MatHang_Anh SET Anh = '{0}' WHERE ID_MHA = {1}",
                        imageString[i],
                        ID_MHA);
                SqlResult res = adapter.ExecuteNonTable(query);
                if(!res.isSuccess)
                    Response.Write("<script>alert('Lỗi: Upload ảnh  " + i + " thất bại');</script>");
            }
        }
        
    }
}