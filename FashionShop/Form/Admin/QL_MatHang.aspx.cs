﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form.Admin
{
    public partial class QL_MatHang : System.Web.UI.Page
    {
        string mode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                Response.Redirect("~/Form/TaiKhoan/Login.aspx");
                return;
            }
            SqlUser user = Session["User"] as SqlUser;
            if (!user.isAdmin)
                Response.Redirect("~/Form/Home.aspx");

            panel_NhapHang.Visible = false;
            if (Request.QueryString["mode"] != null)
                mode = Request.QueryString["mode"];
            if (mode == "xoa")
                XoaMatHang();
            else if (mode == "sua" && (IsPostBack || LoadMatHang()))
            {
                txtTT_btn.Text = "Cập Nhật";
                txtTT_GiaNhap.Enabled = false;
                txtTT_DropDownList.Enabled = false;
                panel_NhapHang.Visible = true;
            }
            else
            {
                mode = "them";
                txtTT_btn.Text = "Tạo Mới";
                txtTT_GiaNhap.Enabled = true;
                txtTT_TonKho.Text = "0";
                if(!IsPostBack)
                    LoadDS_TenNCC();
            }

            if (!IsPostBack)
                Load_DS();
        }
        private void Load_DS(bool isNew = false)
        {
            string page = "0",
                whereName = "",
                order = "MatHang.ID_MH ASC";
            if (!isNew && Request.QueryString["page"] != null)
                page = Request.QueryString["page"];
            if (page == "0")
                txtDS_Pre.Visible = false;
            if (txtDS_Name.Text != "")
                whereName = "where TenMH like N'%" + txtDS_Name.Text + "%'";
            order = txtDS_DropDownList.SelectedValue;

            string query = string.Format("SELECT MatHang.ID_MH, TenMH, NhaCungCap.Ten, GiaNhap, TonKho, MatHang.NgayTao, SUM(DonNhapHang.SoLuong) as 'SoLuong' FROM NhaCungCap inner join MatHang on NhaCungCap.ID_NCC = MatHang.ID_NCC left join DonNhapHang on MatHang.ID_MH = DonNhapHang.ID_MH {0} group by MatHang.ID_MH, TenMH, NhaCungCap.Ten, GiaNhap, TonKho, MatHang.NgayTao ORDER BY {1} OFFSET {2} ROWS FETCH NEXT 20 ROWS ONLY",
                whereName,
                order,
                page);
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow < 20)
                    txtDS_Nex.Visible = false;
                txtDS_Literal.Text = "";
                for (int i = 0; i < res.valueRow; i++)
                {
                    txtDS_Literal.Text += "<tr>";
                    txtDS_Literal.Text += "<td>" + res[i, 0].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 1].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 2].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 3].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 4].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + SqlUnit.ConvertSqlToDate(res[i, 5].ToString()) + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 6].ToString() + "</td>";
                    string suaLink = string.Format("<a href = \"{0}\" class=\"btn btn-info\">Sửa</a>",
                        GetLink("sua", res[i, 0].ToString(), "")),
                        xoaLink = "";
                    int count = 0;
                    int.TryParse(res[i, 6].ToString(), out count);
                    if (count <= 0)
                        xoaLink = string.Format("&nbsp;<a href = \"{0}\" class=\"btn btn-danger\">Xóa</a>",
                        GetLink("xoa", res[i, 0].ToString(), ""));
                    txtDS_Literal.Text += string.Format("<td>{0}{1}</td>",
                        suaLink,
                        xoaLink);
                    txtDS_Literal.Text += "</tr>";
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể load danh sách');</script>");
            adapter.Close();
        }
        protected void txtDS_Btn_Click(object sender, EventArgs e)
        {
            Load_DS(true);
        }
        protected void txtDS_Pre_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            if (page < 20)
                page = 0;
            else
                page -= 20;
            Response.Redirect(GetLink("", "", page.ToString()));
        }
        protected void txtDS_Nex_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            page += 20;
            Response.Redirect(GetLink("", "", page.ToString()));
        }
        private string GetLink(string mode = "", string id = "", string page = "")
        {
            string Mode = "",
                ID = "",
                Page = "";
            if (mode != "")
                Mode = "mode=" + mode;
            else if (Request.QueryString["mode"] != null)
                Mode = "mode=" + Request.QueryString["mode"];
            if (id != "")
                ID = "id=" + id;
            else if (Request.QueryString["id"] != null)
                ID = "id=" + Request.QueryString["id"];
            if (page != "")
                Page = "page=" + page;
            else if (Request.QueryString["page"] != null)
                Page = "page=" + Request.QueryString["page"];

            return Request.Url.AbsolutePath + "?" + Mode + (Mode != "" && (ID != "" || Page != "") ? "&" : "") + ID + ((ID != "" && Mode != "") && Page != "" ? "&" : "") + Page;
        }

        protected void XoaMatHang()
        {
            string ID = "";
            if (Request.QueryString["id"] != null)
                ID = Request.QueryString["id"];
            string query = "DELETE FROM MatHang WHERE ID_MH = " + ID;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                adapter.Close();
                Response.Redirect(GetLink("them", "", ""));
            }
            else
                Response.Write("<script>alert('Lỗi: Xóa thất bại');</script>");
            adapter.Close();
        }
        protected bool LoadMatHang()
        {
            bool bo = false;
            string ID = "";
            if (Request.QueryString["id"] != null)
                ID = Request.QueryString["id"];
            string query = "SELECT ID_MH, CONVERT(varchar(10),NhaCungCap.ID_NCC)  + ' - ' +  NhaCungCap.Ten as 'NCC' , TenMH, GiaNhap, TonKho, MatHang.NgayTao FROM MatHang , NhaCungCap where MatHang.ID_NCC = NhaCungCap.ID_NCC and MatHang.ID_MH = " + ID;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow > 0)
                {
                    txtTT_ID.Text = res[0, 0].ToString();
                    txtTT_DropDownList.Items.Clear();
                    txtTT_DropDownList.Items.Add(res[0, 1].ToString());
                    txtTT_Ten.Text = res[0, 2].ToString();
                    txtTT_GiaNhap.Text = res[0, 3].ToString();
                    txtTT_TonKho.Text = res[0, 4].ToString();
                    txtTT_NT.Text = SqlUnit.ConvertSqlToDate(res[0, 5].ToString());
                    bo = true;
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi  xóa');</script>");
            adapter.Close();
            return bo;
        }
        private void LoadDS_TenNCC()
        {
            txtTT_DropDownList.Items.Clear();
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteTable("SELECT  Ten, ID_NCC FROM NhaCungCap");
            if (res.isSuccess)
            {
                for (int i = 0; i < res.valueRow; i++)
                    txtTT_DropDownList.Items.Add(new ListItem(res[i, 0].ToString(), res[i, 1].ToString()));
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi load danh sách nhà cung cấp');</script>");
            adapter.Close();
        }
        protected void txtTT_btn_Click(object sender, EventArgs e)
        {
            string Ten = txtTT_Ten.Text,
                GiaNhap = txtTT_GiaNhap.Text == "" ? "0": txtTT_GiaNhap.Text,
                NhaCungCap = "";
            if (mode == "them")
            {
                if (txtTT_DropDownList.Items.Count > 0)
                {
                    NhaCungCap = txtTT_DropDownList.SelectedValue;
                    string query = string.Format("INSERT INTO MatHang (ID_NCC, TenMH, GiaNhap) VALUES ( {0}, N'{1}', {2})",
                        NhaCungCap, 
                        Ten, 
                        GiaNhap);
                    SqlAdapter adapter = new SqlAdapter();
                    adapter.Connect();
                    SqlResult res = adapter.ExecuteNonTable(query);
                    if (res.isSuccess)
                    {
                        adapter.Close();
                        Response.Redirect(GetLink("them", "", ""));
                    }
                    else
                        Response.Write("<script>alert('Lỗi: Thất bại khi tạo mới');</script>");
                    adapter.Close();
                }
                else
                {
                    Response.Write("<script>alert('Không thể tạo mới. Lỗi không có nhà cung cấp khả dụng');</script>");
                    return;
                }
            }
            else
            {
                string query = string.Format("UPDATE MatHang SET TenMH = N'{0}' WHERE ID_MH = {1}",
                    Ten, txtTT_ID.Text);
                SqlAdapter adapter = new SqlAdapter();
                adapter.Connect();
                SqlResult res = adapter.ExecuteNonTable(query);
                if (res.isSuccess)
                {
                    adapter.Close();
                    Response.Redirect(GetLink("them", "", ""));
                }
                else
                    Response.Write("<script>alert('Lỗi: Thất bại khi tạo mới');</script>");
                adapter.Close();
            }
        }
        protected void txtNH_btn_Click(object sender, EventArgs e)
        {
            int soluong = 0;
            int.TryParse(txtNH_SoNhap.Text.Trim(), out soluong);
            if(soluong <= 0)
                Response.Write("<script>alert('Lượng nhập phải lớn hơn 0');</script>");
            string ghichu = txtNH_GC.Text.Trim();
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            string query = string.Format("INSERT INTO DonNhapHang (ID_MH, ID_NV, SoLuong, GhiChu) VALUES({0}, {1}, {2}, N'{3}'); update MatHang set TonKho = TonKho + {2} where ID_MH = {0};",
                txtTT_ID.Text, (Session["User"] as SqlUser).ID, soluong, ghichu
                );
            SqlResult res =  adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                adapter.Close();
                Response.Redirect(Request.Url.PathAndQuery);
            }
            else
                Response.Write("<script>alert('Lỗi: Nhập hàng thất bại');</script>");
            adapter.Close();
        }
    }
}