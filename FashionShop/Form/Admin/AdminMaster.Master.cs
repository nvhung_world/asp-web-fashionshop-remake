﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form.Admin
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                Response.Redirect("~/Form/TaiKhoan/Login.aspx");
                return;
            }
            SqlUser user = Session["User"] as SqlUser;
            if (user.isOnlyKhachHang)
                Response.Redirect("~/Form/Home.aspx");
        }
    }
}