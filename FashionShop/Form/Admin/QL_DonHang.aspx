﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Form/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="QL_DonHang.aspx.cs" Inherits="FashionShop.Form.Admin.QL_DonHang" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="panel panel-success" style="width: 50%; margin:20px auto auto auto">
        <div class="panel panel-heading">
            Thông Tin Nhà Cung Cấp
        </div>
        <div class="panel panel-body">
            <div class="row" style="width:80%; margin: auto auto auto auto">
                <div class="col-sm-1">
                    ID:
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="txtTT_ID" runat="server"></asp:Label>
                </div>
                <div class="col-sm-3">
                    Ngày Tạo:
                </div>
                <div class="col-sm-5">
                    <asp:Label ID="txtTT_NT" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-sm-3">
                    Tên
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_Ten" runat="server" CssClass="form-control" MaxLength="30" Width="100%"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-3">
                    Số ĐT
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_SDT" runat="server" CssClass="form-control" MaxLength="11" Width="100%" TextMode="Phone"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-3">
                    Email
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_Email" runat="server" CssClass="form-control" MaxLength="50" Width="100%" TextMode="Email"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-3">
                    Địa Chỉ
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_DC" runat="server" CssClass="form-control NoResize" Rows="3" MaxLength="100" Width="100%" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-7">
                </div>
                <div class="col-sm-5">
                    <asp:Button ID="txtTT_btn" runat="server" Text="Tạo Mới" OnClick="txtTT_btn_Click" CssClass="btn btn-primary" Width="100%"/>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-success" style="width: 90%; margin:20px auto auto auto">
        <div class="panel panel-heading">
            Danh sách Nhà Cung Cấp
        </div>
        <div class="panel panel-body">
            <div class="row" style="width:90%;margin: auto auto auto auto">
                <div class="col-sm-1" style="text-align:right">
                    Tên
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtDS_Name" runat="server" CssClass="form-control" Width="100%" ></asp:TextBox>
                </div>
                <div class="col-sm-2" style="text-align:right">
                    Xắp xếp
                </div>
                <div class="col-sm-3">
                    <asp:DropDownList ID="txtDS_DropDownList" runat="server" CssClass="form-control" Width="100%">
                        <asp:ListItem Text="ID Tăng" Value="NhaCungCap.ID_NCC ASC"></asp:ListItem>
                        <asp:ListItem Text="ID Giảm" Value="NhaCungCap.ID_NCC Desc"></asp:ListItem>
                        <asp:ListItem Text="Theo Tên Tăng" Value="Ten ASC"></asp:ListItem>
                        <asp:ListItem Text="Theo Tên Giảm" Value="Ten Desc"></asp:ListItem>
                        <asp:ListItem Text="Ngày Tạo Tăng" Value="NhaCungCap.NgayTao ASC"></asp:ListItem>
                        <asp:ListItem Text="Ngày Tạo Giảm" Value="NhaCungCap.NgayTao Desc"></asp:ListItem>
                        <asp:ListItem Text="Đơn Hàng Tăng" Value="'Tong DH' ASC"></asp:ListItem>
                        <asp:ListItem Text="Đơn Hàng Giảm" Value="'Tong DH' Desc"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="txtDS_Btn" runat="server" Text="Tìm Kiếm" CssClass="btn btn-info" Width="100%" OnClick="txtDS_Btn_Click"/>
                </div>
            </div>
            <table class="table">
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Tên Nhà Cung Cấp
                    </th>
                    <th>
                        Điện Thoại
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Địa Chỉ
                    </th>
                    <th>
                        Ngày Tạo
                    </th>
                    <th>
                        Số đơn hàng
                    </th>
                    <th>
                    </th>
                </tr>
                <asp:Literal ID="txtDS_Literal" runat="server"></asp:Literal>
            </table>
            <div style="margin: 20px auto 10px auto">
                <div class="row">
                    <div class="col-sm-2">

                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="txtDS_Pre" runat="server" Font-Bold="true" Text="< Trước" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Pre_Click"/>
                    </div>
                    <div class="col-sm-2">

                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="txtDS_Nex" runat="server" Font-Bold="true" Text="Sau >" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Nex_Click"/>
                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
