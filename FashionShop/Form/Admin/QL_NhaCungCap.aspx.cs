﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form.Admin
{
    public partial class QL_NhaCungCap : System.Web.UI.Page
    {
        string mode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                Response.Redirect("~/Form/TaiKhoan/Login.aspx");
                return;
            }
            SqlUser user = Session["User"] as SqlUser;
            if (!user.isAdmin)
                Response.Redirect("~/Form/Home.aspx");

            if (Request.QueryString["mode"] != null)
                mode = Request.QueryString["mode"];
            if (mode == "xoa")
                XoaNCC();
            else if (mode == "sua" && (IsPostBack || LoadNCC()))
            {
                txtTT_btn.Text = "Cập Nhật";
            }
            else
            {
                mode = "them";
                txtTT_btn.Text = "Tạo Mới";
            }

            if (!IsPostBack)
                Load_DS();
        }
        private void Load_DS(bool isNew = false)
        {
            string page = "0",
                whereName = "",
                order = "nhacungcap.ID_NCC ASC";
            if (!isNew && Request.QueryString["page"] != null)
                page = Request.QueryString["page"];
            if (page == "0")
                txtDS_Pre.Visible = false;
            if(txtDS_Name.Text != null)
                whereName = "where Ten like N'%"+ txtDS_Name.Text + "%'";
            order = txtDS_DropDownList.SelectedValue;

            string query = string.Format("SELECT nhacungcap.ID_NCC, Ten, SDT, Email, DiaChi , NhaCungCap.NgayTao, COUNT(ID_DNH) as 'Tong DH' FROM NhaCungCap left join MatHang on NhaCungCap.ID_NCC = MatHang.ID_NCC left join DonNhapHang on MatHang.ID_MH = DonNhapHang.ID_MH {0} group by nhacungcap.ID_NCC, Ten, SDT, Email, DiaChi , NhaCungCap.NgayTao ORDER BY {1} OFFSET {2} ROWS FETCH NEXT 20 ROWS ONLY",
                whereName,
                order,
                page);
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteTable(query);
            if(res.isSuccess)
            {
                if (res.valueRow < 20)
                    txtDS_Nex.Visible = false;
                txtDS_Literal.Text = "";
                for (int i=0 ;i < res.valueRow; i++)
                {
                    txtDS_Literal.Text += "<tr>";
                    txtDS_Literal.Text += "<td>" + res[i, 0].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 1].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 2].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 3].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 4].ToString() + "</td>";
                    txtDS_Literal.Text += "<td>" + SqlUnit.ConvertSqlToDate(res[i, 5].ToString()) + "</td>";
                    txtDS_Literal.Text += "<td>" + res[i, 6].ToString() + "</td>";
                    string suaLink = string.Format("<a href = \"{0}\" class=\"btn btn-info\">Sửa</a>",
                        GetLink("sua", res[i, 0].ToString(), "")),
                        xoaLink = "";
                    int count = 0;
                    int.TryParse(res[i, 6].ToString(), out count);
                    if (count <= 0)
                        xoaLink = string.Format("&nbsp;<a href = \"{0}\" class=\"btn btn-danger\">Xóa</a>",
                        GetLink("xoa", res[i, 0].ToString(), ""));
                    txtDS_Literal.Text += string.Format("<td>{0}{1}</td>",
                        suaLink,
                        xoaLink);
                    txtDS_Literal.Text += "</tr>";
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Không thể load danh sách');</script>");
            adapter.Close();
        }
        protected void txtDS_Btn_Click(object sender, EventArgs e)
        {
            Load_DS(true);
        }
        protected void txtDS_Pre_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse( Request.QueryString["page"], out page);
            if (page < 20)
                page = 0;
            else
                page -= 20;
            Response.Redirect(GetLink("", "", page.ToString()));
        }
        protected void txtDS_Nex_Click(object sender, EventArgs e)
        {
            int page = 0;
            if (Request.QueryString["page"] != null)
                int.TryParse(Request.QueryString["page"], out page);
            page += 20;
            Response.Redirect(GetLink("", "", page.ToString()));
        }
        private string GetLink(string mode = "", string id = "", string page = "")
        {
            string Mode = "",
                ID = "",
                Page = "";
            if (mode != "")
                Mode = "mode=" + mode;
            else if (Request.QueryString["mode"] != null)
                Mode = "mode=" + Request.QueryString["mode"];
            if (id != "")
                ID = "id=" + id;
            else if (Request.QueryString["id"] != null)
                ID = "id=" + Request.QueryString["id"];
            if (page != "")
                Page = "page=" + page;
            else if (Request.QueryString["page"] != null)
                Page = "page=" + Request.QueryString["page"];

            return Request.Url.AbsolutePath + "?" + Mode + (Mode != "" && (ID != "" || Page != "") ? "&": "") + ID + ((ID != "" && Mode != "") && Page != "" ? "&" : "") + Page;
        }

        protected void XoaNCC()
        {
            string ID = "";
            if (Request.QueryString["id"] != null)
                ID = Request.QueryString["id"];
            string query = "DELETE FROM NhaCungCap WHERE ID_NCC = " + ID;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                adapter.Close();
                Response.Redirect(GetLink("them", "", ""));
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi xóa');</script>");
            adapter.Close();
        }
        protected bool LoadNCC()
        {
            bool bo = false;
            string ID = "";
            if (Request.QueryString["id"] != null)
                ID = Request.QueryString["id"];
            string query = "SELECT ID_NCC, Ten, SDT, Email, DiaChi, NgayTao FROM NhaCungCap where ID_NCC = " + ID;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if(res.valueRow > 0)
                {
                    txtTT_ID.Text = res[0, 0].ToString();
                    txtTT_Ten.Text = res[0, 1].ToString();
                    txtTT_SDT.Text = res[0, 2].ToString();
                    txtTT_Email.Text = res[0, 3].ToString();
                    txtTT_DC.Text = res[0, 4].ToString();
                    txtTT_NT.Text = SqlUnit.ConvertSqlToDate(res[0, 5].ToString());
                    bo = true;
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi xóa');</script>");
            adapter.Close();
            return bo;
        }
        protected void txtTT_btn_Click(object sender, EventArgs e)
        {
            string Ten = txtTT_Ten.Text,
                SDT = txtTT_SDT.Text,
                Email = txtTT_Email.Text,
                DiaChi = txtTT_DC.Text;
            if(mode == "them")
            {
                string query = string.Format("INSERT INTO NhaCungCap(Ten, SDT, Email, DiaChi) VALUES(N'{0}', '{1}', N'{2}', N'{3}')",
                    Ten, SDT, Email, DiaChi);
                SqlAdapter adapter = new SqlAdapter();
                adapter.Connect();
                SqlResult res = adapter.ExecuteNonTable(query);
                if(res.isSuccess)
                {
                    adapter.Close();
                    Response.Redirect(GetLink("them", "", ""));

                }
                else
                    Response.Write("<script>alert('Lỗi: Thất bại khi tạo mới');</script>");
                adapter.Close();
            }
            else
            {
                string query = string.Format("UPDATE NhaCungCap SET Ten = N'{0}', SDT = '{1}',Email = N'{2}', DiaChi = N'{3}' WHERE ID_NCC ={4}",
                    Ten, SDT, Email, DiaChi, txtTT_ID.Text);
                SqlAdapter adapter = new SqlAdapter();
                adapter.Connect();
                SqlResult res = adapter.ExecuteNonTable(query);
                if (res.isSuccess)
                {
                    adapter.Close();
                    Response.Redirect(GetLink("them", "", ""));
                }
                else
                    Response.Write("<script>alert('Lỗi: Thất bại khi tạo mới');</script>");
                adapter.Close();
            }
        }
    }
}