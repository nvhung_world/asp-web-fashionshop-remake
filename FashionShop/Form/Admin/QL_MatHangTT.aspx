﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Form/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="QL_MatHangTT.aspx.cs" Inherits="FashionShop.Form.Admin.QL_MatHangTT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>
        Quản Lý Thông Tin Mặt Hàng
    </title>
    <style>
        .Fit{
            object-fit: contain;
        }
    </style>
    <script type="text/javascript">
        function showBrowseDialog1() {
            var file = document.getElementById('<%=FileUpload1.ClientID%>');
            file.click();
            file.onchange = function (e) {
                $('#<%=Image1.ClientID%>').attr('src', URL.createObjectURL(e.target.files[0]));
            };
        }
        function showBrowseDialog2() {
            var file = document.getElementById('<%=FileUpload2.ClientID%>');
            file.click();
            file.onchange = function (e) {
                $('#<%=Image2.ClientID%>').attr('src', URL.createObjectURL(e.target.files[0]));
            };
        }
        function showBrowseDialog3() {
            var file = document.getElementById('<%=FileUpload3.ClientID%>');
            file.click();
            file.onchange = function (e) {
                $('#<%=Image3.ClientID%>').attr('src', URL.createObjectURL(e.target.files[0]));
            };
        }
        function showBrowseDialog4() {
            var file = document.getElementById('<%=FileUpload4.ClientID%>');
            file.click();
            file.onchange = function (e) {
                $('#<%=Image4.ClientID%>').attr('src', URL.createObjectURL(e.target.files[0]));
            };
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <asp:Panel ID="panelTT" runat="server">
        <div class="panel panel-success" style="width: 60%; margin:20px auto auto auto">
            <div class="panel panel-heading">
                Thông Tin Mặt Hàng
            </div>
            <div class="panel panel-body">
                <div>
                    <div class="row">
                            <div class="col-sm-2">
                                ID
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="txtTT_ID" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-2">
                                Ngày Tạo
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="txtTT_NT" runat="server"></asp:Label>
                            </div>
                        </div>
                    <div class="row" style="margin-top: 10px">
                            <div class="col-sm-2">
                                Tên Mặt Hàng
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="txtTT_TenMH" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-2">
                                Tồn Kho
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="txtTT_TonKho" runat="server"></asp:Label>
                            </div>
                        </div>
                    <div class="row" style="margin-top: 10px">
                            <div class="col-sm-2 ">
                                Lượt Xem
                                <span class="glyphicon glyphicon-search"></span>
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="txtTT_LuotXem" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-2">
                                Lượt thích
                                <span class="glyphicon glyphicon-heart" style="color:red"></span>
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="txtTT_LuotThich" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    <div class="row" style="margin-top: 10px">
                            <div class="col-sm-2">
                                Giá Bán
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTT_GiaBan" runat="server" CssClass="form-control" Width="100%" TextMode="Number"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                Đang Bán
                            </div>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="txtTT_DangBan" runat="server" Checked="true"/>
                            </div>
                        </div>
                    <div class="row" style="margin-top: 5px">
                            <div class="col-sm-2">
                                Thương Hiệu
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTT_ThuongHieu" runat="server" CssClass="form-control" MaxLength="50" Width="100%"></asp:TextBox>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    <div class="row" style="margin-top: 5px">
                            <div class="col-sm-2">
                                Năm Sản Xuất
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTT_NanSX" runat="server" CssClass="form-control" MaxLength="100" Width="100%" TextMode="Number"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                Đối Tượng
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTT_DoiTuong" runat="server" CssClass="form-control" MaxLength="20" Width="100%" ></asp:TextBox>
                            </div>
                        </div>
                    <div class="row" style="margin-top: 5px">
                            <div class="col-sm-2">
                                Loại
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTT_Loai" runat="server" CssClass="form-control" MaxLength="30" Width="100%" ></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                Chất Liệu
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtTT_ChatLieu" runat="server" CssClass="form-control" MaxLength="20" Width="100%" ></asp:TextBox>
                            </div>
                        </div>
                    <div class="row" style="margin-top: 5px">
                            <div class="col-sm-2">
                                Mô Tả
                            </div>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTT_MoTa" runat="server" CssClass="form-control NoResize" MaxLength="1000" Width="100%" TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </div>
                        </div>
                </div>
                <div class="row"  style="margin-top: 10px">
                    <div class="col-sm-3">
                        <div>
                                <asp:Image ID="Image1" runat="server" CssClass="img-thumbnail Fit" alt="Cinque Terre" ImageUrl="~/Image/NoImage-icon.png" Width="100%" ImageAlign="Middle" Height="170px"/>
                            </div>
                            <div>
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="hidden"/>
                                <input type="button" value="Chọn Ảnh" class="btn btn-default btn-sm" style="width:100%;margin-top:5px;font-weight:bolder" onclick="showBrowseDialog1()"/>
                            </div>
                    </div>
                    <div class="col-sm-3">
                        <div>
                            <asp:Image ID="Image2" runat="server" CssClass="img-thumbnail Fit" alt="Cinque Terre" ImageUrl="~/Image/NoImage-icon.png" Width="100%" ImageAlign="Middle" Height="170px"/>
                            </div >
                            <div>
                                <asp:FileUpload ID="FileUpload2" runat="server" CssClass="hidden"/>
                                <input type="button" value="Chọn Ảnh" class="btn btn-default btn-sm" style="width:100%;margin-top:5px;font-weight:bolder" onclick="showBrowseDialog2()"/>
                            </div>
                    </div>
                    <div class="col-sm-3">
                        <div>
                                <asp:Image ID="Image3" runat="server" CssClass="img-thumbnail Fit" alt="Cinque Terre" ImageUrl="~/Image/NoImage-icon.png" Width="100%" ImageAlign="Middle" Height="170px"/>
                            </div >
                            <div>
                                <asp:FileUpload ID="FileUpload3" runat="server" CssClass="hidden"/>
                                <input type="button" value="Chọn Ảnh" class="btn btn-default btn-sm" style="width:100%;margin-top:5px;font-weight:bolder" onclick="showBrowseDialog3()"/>
                            </div>
                    </div>
                    <div class="col-sm-3">
                        <div>
                                <asp:Image ID="Image4" runat="server" CssClass="img-thumbnail Fit" alt="Cinque Terre" ImageUrl="~/Image/NoImage-icon.png" Width="100%" ImageAlign="Middle" Height="170px"/>
                            </div >
                            <div>
                                <asp:FileUpload ID="FileUpload4" runat="server" CssClass="hidden"/>
                                <input type="button" value="Chọn Ảnh" class="btn btn-default btn-sm" style="width:100%;margin-top:5px;font-weight:bolder" onclick="showBrowseDialog4()"/>
                            </div>
                    </div>
                </div>
                <div style="margin-top: 15px">
                    <asp:Button ID="txtTT_btn" runat="server" Text="Cập Nhật" OnClick="txtTT_btn_Click" CssClass="btn btn-primary" Width="100%"/>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="panel panel-success" style="width: 90%; margin:20px auto auto auto">
        <div class="panel panel-heading">
            Danh sách Nhà Cung Cấp
        </div>
        <div class="panel panel-body">
            <div class="row" style="width:90%;margin: auto auto auto auto">
                <div class="col-sm-1" style="text-align:right">
                    Tên
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtDS_Name" runat="server" CssClass="form-control" Width="100%" ></asp:TextBox>
                </div>
                <div class="col-sm-2" style="text-align:right">
                    Xắp xếp
                </div>
                <div class="col-sm-3">
                    <asp:DropDownList ID="txtDS_DropDownList" runat="server" CssClass="form-control" Width="100%">
                        <asp:ListItem Text="ID Tăng" Value="ID_MHTT ASC"></asp:ListItem>
                        <asp:ListItem Text="ID Giảm" Value="ID_MHTT Desc"></asp:ListItem>
                        <asp:ListItem Text="Theo Tên Tăng" Value="TenMH ASC"></asp:ListItem>
                        <asp:ListItem Text="Theo Tên Giảm" Value="TenMH Desc"></asp:ListItem>
                        <asp:ListItem Text="Lượt Thích Tăng" Value="LuotThich ASC"></asp:ListItem>
                        <asp:ListItem Text="Lượt Thích Giảm" Value="LuotThich Desc"></asp:ListItem>
                        <asp:ListItem Text="Giái Bán Tăng" Value="GiaBan ASC"></asp:ListItem>
                        <asp:ListItem Text="Giái Bán Giảm" Value="GiaBan Desc"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="txtDS_Btn" runat="server" Text="Tìm Kiếm" CssClass="btn btn-info" Width="100%" OnClick="txtDS_Btn_Click"/>
                </div>
            </div>
            <table class="table">
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Tên Mặt Hàng
                    </th>
                    <th>
                        Tồn Kho
                    </th>
                    <th>
                        Giá Bán
                    </th>
                    <th>
                        Đang Bán
                    </th>
                    <th>
                        Thương Hiệu
                    </th>
                    <th>
                        Lượt Xem
                    </th>
                    <th>
                        Lượt Thích
                    </th>
                    <th>
                        Năm Sản xuất
                    </th>
                    <th>
                        Đối Tương
                    </th>
                    <th>
                        Loại
                    </th>
                    <th>
                        Chất Liệu
                    </th>
                    <th>
                    </th>
                </tr>
                <asp:Literal ID="txtDS_Literal" runat="server"></asp:Literal>
            </table>
            <div style="margin: 20px auto 10px auto">
                <div class="row">
                    <div class="col-sm-2">

                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="txtDS_Pre" runat="server" Font-Bold="true" Text="< Trước" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Pre_Click"/>
                    </div>
                    <div class="col-sm-2">

                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="txtDS_Nex" runat="server" Font-Bold="true" Text="Sau >" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Nex_Click"/>
                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
