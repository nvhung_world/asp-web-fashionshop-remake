﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Form/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="QL_MatHang.aspx.cs" Inherits="FashionShop.Form.Admin.QL_MatHang" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>
        Quản Lý Kho Hàng
    </title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="panel panel-info" style="width: 50%; margin:20px auto auto auto">
        <div class="panel panel-heading">
            Mặt Hàng
        </div>
        <div class="panel panel-body">
            <div class="row" style="width:80%; margin: auto auto auto auto">
                <div class="col-sm-1">
                    ID:
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="txtTT_ID" runat="server"></asp:Label>
                </div>
                <div class="col-sm-3">
                    Ngày Tạo:
                </div>
                <div class="col-sm-5">
                    <asp:Label ID="txtTT_NT" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-sm-3">
                    Nhà Cung Cấp
                </div>
                <div class="col-sm-9">
                    <asp:DropDownList ID="txtTT_DropDownList" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-sm-3">
                    Tên Mặt Hàng
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_Ten" runat="server" CssClass="form-control" MaxLength="30" Width="100%"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-3">
                    Giá Nhập
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_GiaNhap" runat="server" CssClass="form-control" MaxLength="11" Width="100%" TextMode="Number"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-3">
                    Tồn Kho
                </div>
                <div class="col-sm-9">
                    <asp:TextBox ID="txtTT_TonKho" runat="server" CssClass="form-control active" MaxLength="50" Width="100%" TextMode="Number" Enabled="false"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-sm-7">
                </div>
                <div class="col-sm-5">
                    <asp:Button ID="txtTT_btn" runat="server" Text="Tạo Mới" CssClass="btn btn-primary" Width="100%" OnClick="txtTT_btn_Click"/>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="panel_NhapHang" runat="server" style="width: 50%; margin:20px auto auto auto">
        <div class="panel panel-info">
            <div class="panel panel-heading" >
                Nhập Hàng
            </div>
            <div class ="panel panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        Số Lượng Nhập
                    </div>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNH_SoNhap" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                    </div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-sm-3">
                         Ghi Chú
                    </div>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNH_GC" runat="server" CssClass="form-control NoResize" Rows="3" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-sm-7">
                    </div>
                    <div class="col-sm-5">
                        <asp:Button ID="txtNH_btn" runat="server" Text="Nhập Hàng" CssClass="btn btn-warning" Width="100%" OnClick="txtNH_btn_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="panel panel-info" style="width: 90%; margin:20px auto auto auto">
        <div class="panel panel-heading">
            Danh sách Nhà Cung Cấp
        </div>
        <div class="panel panel-body">
            <div class="row" style="width:90%;margin: auto auto auto auto">
                <div class="col-sm-1" style="text-align:right">
                    Tên
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtDS_Name" runat="server" CssClass="form-control" Width="100%" ></asp:TextBox>
                </div>
                <div class="col-sm-2" style="text-align:right">
                    Xắp xếp
                </div>
                <div class="col-sm-3">
                    <asp:DropDownList ID="txtDS_DropDownList" runat="server" CssClass="form-control" Width="100%">
                        <asp:ListItem Text="ID Tăng" Value="MatHang.ID_MH ASC"></asp:ListItem>
                        <asp:ListItem Text="ID Giảm" Value="MatHang.ID_MH Desc"></asp:ListItem>
                        <asp:ListItem Text="Theo Tên Tăng" Value="TenMH ASC"></asp:ListItem>
                        <asp:ListItem Text="Theo Tên Giảm" Value="TenMH Desc"></asp:ListItem>
                        <asp:ListItem Text="Tên NhàCC Tăng" Value="NhaCungCap.Ten ASC"></asp:ListItem>
                        <asp:ListItem Text="Tên NhàCC Giảm" Value="NhaCungCap.Ten Desc"></asp:ListItem>
                        <asp:ListItem Text="Giá Nhập Tăng" Value="GiaNhap ASC"></asp:ListItem>
                        <asp:ListItem Text="Giá Nhập Giảm" Value="GiaNhap Desc"></asp:ListItem>
                        <asp:ListItem Text="Tồn kho Tăng" Value="TonKho ASC"></asp:ListItem>
                        <asp:ListItem Text="Tồn kho Giảm" Value="TonKho Desc"></asp:ListItem>
                        <asp:ListItem Text="Ngày Tạo Tăng" Value="MatHang.NgayTao ASC"></asp:ListItem>
                        <asp:ListItem Text="Ngày Tạo Giảm" Value="MatHang.NgayTao Desc"></asp:ListItem>
                        <asp:ListItem Text="Số Mặt Hàng Tăng" Value="'SoLuong' ASC"></asp:ListItem>
                        <asp:ListItem Text="Số Mặt Hàng Giảm" Value="'SoLuong' Desc"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="txtDS_Btn" runat="server" Text="Tìm Kiếm" CssClass="btn btn-info" Width="100%" OnClick="txtDS_Btn_Click"/>
                </div>
            </div>
            <table class="table">
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Tên Mặt Hàng
                    </th>
                    <th>
                        Nhà Cung Cấp
                    </th>
                    <th>
                        Giá Nhập
                    </th>
                    <th>
                        Tồn Kho
                    </th>
                    <th>
                        Ngày Tạo
                    </th>
                    <th>
                        Số Sản Phẩm
                    </th>
                    <th>
                    </th>
                </tr>
                <asp:Literal ID="txtDS_Literal" runat="server"></asp:Literal>
            </table>
            <div style="margin: 20px auto 10px auto">
                <div class="row">
                    <div class="col-sm-2">

                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="txtDS_Pre" runat="server" Font-Bold="true" Text="< Trước" CssClass="btn btn-success" Width="100%" OnClick="txtDS_Pre_Click"/>
                    </div>
                    <div class="col-sm-2">

                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="txtDS_Nex" runat="server" Font-Bold="true" Text="Sau >"  CssClass="btn btn-success" Width="100%" OnClick="txtDS_Nex_Click"/>
                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
