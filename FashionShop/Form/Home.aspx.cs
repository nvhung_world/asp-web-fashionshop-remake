﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;

            string s = txtDS_DropPage.SelectedValue;
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            if(!IsPostBack)
                Load_BoLoc(adapter);
            Load_Count_DS(adapter);
            Load_DS(adapter);
            adapter.Close();
        }
        private void Load_BoLoc(SqlAdapter adapter)
        {
            string query = "";
            SqlResult res;
            //.............Thương Hiệu......................
            query = "SELECT Top 10 ThuongHieu FROM MatHang_TT left join DonHang_CT on ID_MHTT = ID_MH  where ThuongHieu not like '' group by ThuongHieu order by sum(SoLuong) asc";
            res = adapter.ExecuteTable(query);
            if(res.isSuccess)
            {
                txtBL_ThuongHieu.Items.Clear();
                for (int i = 0; i < res.valueRow; i++)
                    txtBL_ThuongHieu.Items.Add(new ListItem(res[i,0].ToString(), " MatHang_TT.ThuongHieu= N'" + res[i, 0].ToString() + "'"));
            }
            //.............Đối tượng......................
            query = "SELECT DISTINCT TOP 10 DoiTuong FROM MatHang_TT  where DoiTuong not like ''";
            res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                txtBL_DoiTuong.Items.Clear();
                for (int i = 0; i < res.valueRow; i++)
                    txtBL_DoiTuong.Items.Add(new ListItem(res[i, 0].ToString(), " MatHang_TT.DoiTuong = N'" + res[i, 0].ToString() + "'"));
            }
            //.............Chất Liệu......................
            query = "SELECT DISTINCT TOP 10 ChatLieu FROM MatHang_TT  where ChatLieu not like ''";
            res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                txtBL_ChatLieu.Items.Clear();
                for (int i = 0; i < res.valueRow; i++)
                    txtBL_ChatLieu.Items.Add(new ListItem(res[i, 0].ToString(), " MatHang_TT.ChatLieu = N'" + res[i, 0].ToString() + "'"));
            }
            //.............Loại Sử Dụng......................
            query = "SELECT DISTINCT TOP 10 Loai FROM MatHang_TT where Loai not like ''";
            res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                txtBL_ChatLieu.Items.Clear();
                for (int i = 0; i < res.valueRow; i++)
                    txtBL_ChatLieu.Items.Add(new ListItem(res[i, 0].ToString(), " MatHang_TT.Loai = N'" + res[i, 0].ToString() + "'"));
            }
        }
        private void Load_Count_DS(SqlAdapter adapter)
        {
            string query = string.Format("SELECT count(ID_MHTT) FROM MatHang_TT inner join MatHang on MatHang_TT.ID_MHTT = MatHang.ID_MH left join MatHang_Anh on (MatHang.ID_MH = MatHang_Anh.ID_MH and MatHang_Anh.Loai = 0) where MatHang_TT.DangBan = 1 {0}",
                GetSqlWhere());
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                int oldSelectIndex = txtDS_DropPage.SelectedIndex < 0 ? 0 : txtDS_DropPage.SelectedIndex;
                txtDS_DropPage.Items.Clear();
                int count =  0;
                int.TryParse(res[0, 0].ToString(), out count);
                for (int i = 0; i <= count / 12; i++)
                    txtDS_DropPage.Items.Add(new ListItem(i.ToString(), (i * 12).ToString()));
                txtDS_DropPage.SelectedIndex = oldSelectIndex > txtDS_DropPage.Items.Count ? txtDS_DropPage.Items.Count : oldSelectIndex;
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi Load danh sách mặt hàng');</script>");
        }
        private void Load_DS(SqlAdapter adapter)
        {
            MatHangIcon[] MH_Icons = new MatHangIcon[] { txtDS_MHIcon_1, txtDS_MHIcon_2, txtDS_MHIcon_3, txtDS_MHIcon_4, txtDS_MHIcon_5, txtDS_MHIcon_6, txtDS_MHIcon_7, txtDS_MHIcon_8, txtDS_MHIcon_9, txtDS_MHIcon_10, txtDS_MHIcon_11, txtDS_MHIcon_12 };
            string query = string.Format("SELECT ID_MHTT, Anh, GiaBan, TenMH, TonKho, ThuongHieu FROM MatHang_TT inner join MatHang on MatHang_TT.ID_MHTT = MatHang.ID_MH left join MatHang_Anh on (MatHang.ID_MH = MatHang_Anh.ID_MH and MatHang_Anh.Loai = 0) where MatHang_TT.DangBan = 1 {0} ORDER BY {1} OFFSET {2} ROWS FETCH NEXT 12 ROWS ONLY",
                GetSqlWhere(),
                GetSqlOrder(),
                txtDS_DropPage.SelectedValue);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                for(int i=0;i<12;i++)
                {
                    if(i< res.valueRow)
                    {
                        MH_Icons[i].Visible = true;
                        MH_Icons[i].ID_MH = res[i, 0].ToString();
                        string imageUrl = SqlUnit.convertStringToImageUrl(res[i, 1].ToString());
                        if (imageUrl != "")
                            MH_Icons[i].ImageUrl = imageUrl;
                        else
                            MH_Icons[i].ImageUrl = "../Image/NoImage-icon.png";
                        MH_Icons[i].GiaBan = res[i, 2].ToString();
                        MH_Icons[i].TenMH = res[i, 3].ToString();
                        MH_Icons[i].isOK = res[i, 4].ToString() == "" ? false: true ;
                        MH_Icons[i].ThuongHieu = res[i, 5].ToString();
                    }
                    else
                        MH_Icons[i].Visible = false;
                }
            }
            else
                Response.Write("<script>alert('Lỗi: Thất bại khi Load danh sách mặt hàng');</script>");
        }
        private string GetSqlWhere()
        {
            string[] var = new string[5] { "", "" , "", "" ,""};
            if(Request.QueryString["NameTxt"] != null)
                var[0] = string.Format("(TenMH like N'%{0}%' or ThuongHieu like N'%{0}%')",
                    Request.QueryString["NameTxt"]);
            CheckBoxList[] checks = new[] { txtBL_ThuongHieu, txtBL_DoiTuong, txtBL_ChatLieu, txtBL_Loai };
            for(int i=0;i<4;i++)
            {
                for(int j=0; j < checks[i].Items.Count; j++)
                {
                    if(checks[i].Items[j].Selected)
                    {
                        if (var[i+1] == "")
                            var[i+1] = "(" + checks[i].Items[j].Value;
                        else
                            var[i+1] += " or " + checks[i].Items[j].Value;
                    }
                }
                if (var[i+1] != "")
                    var[i+1] += ")";
            }
            string res = "";
            for (int i = 0; i < 5; i++)
            {
                if(var[i] != "")
                    res += " and " + var[i];
            }
            return res;
        }
        private string GetSqlOrder()
        {
            if (IsPostBack)
                return txtDS_SapXep.SelectedValue;
            else if (Request.QueryString["order"] != null)
            {
                if (Request.QueryString["order"] == "Price")
                    return " GiaBan desc ";
                else if (Request.QueryString["order"] == "Trending")
                    return " LuotThich/(case when LuotXem is null then 1 when LuotXem <= 0 then 1 else LuotXem end) desc ";
                else if (Request.QueryString["order"] == "New")
                    return " MatHang.NgayTao desc ";
            }
            return " TenMH asc ";
        }
    }
}