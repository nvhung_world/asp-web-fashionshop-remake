﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Form/HomeMaster.Master" AutoEventWireup="true" CodeBehind="MatHangTT.aspx.cs" Inherits="FashionShop.Form.MatHangTT" %>

<%@ Register Src="~/Form/MatHangIcon.ascx" TagPrefix="uc1" TagName="MatHangIcon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>
        Thông Tin Mặt Hàng
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-sm-9">
            <div class="panel panel-warning">
                <div class="panel panel-heading" style="font-size: 30px; font-weight: bolder; color: #0d55ff">
                    <asp:Label ID="txt_TenMH" runat="server" Text="Tên Mặt Hàng"></asp:Label>
                </div>
                <div class="panel panel-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                    <li data-target="#myCarousel" data-slide-to="3"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div style="width:70%;margin : auto 15% auto 15%">
                                            <asp:Image ID="txt_Image1" runat="server"  ImageUrl="~/Image/NoImage-icon.png" CssClass="img-rounded Fit" Width="100%" Height="300px"  ImageAlign="Middle"/>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div style="width:70%;margin : auto 15% auto 15%">
                                            <asp:Image ID="txt_Image2" runat="server"  ImageUrl="~/Image/NoImage-icon.png" CssClass="img-rounded Fit" Width="100%" Height="300px"  ImageAlign="Middle"/>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div style="width:70%;margin : auto 15% auto 15%">
                                            <asp:Image ID="txt_Image3" runat="server"  ImageUrl="~/Image/NoImage-icon.png" CssClass="img-rounded Fit" Width="100%" Height="300px"  ImageAlign="Middle"/>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div style="width:70%;margin : auto 15% auto 15%">
                                            <asp:Image ID="txt_Image4" runat="server"  ImageUrl="~/Image/NoImage-icon.png" CssClass="img-rounded Fit" Width="100%" Height="300px"  ImageAlign="Middle"/>
                                        </div>
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <div style="padding-bottom: 10px">
                                                Giá Bán
                                            </div>
                                        </th>
                                        <th colspan="3" style="font-size:30px; text-align:center;color:red">
                                            <asp:Label ID="txt_GiaBan" runat="server"></asp:Label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>
                                            Thương Hiệu
                                        </th>
                                        <td colspan="3">
                                            <asp:Label ID="txt_ThuongHieu" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Đối Tượng
                                        </th>
                                        <td colspan="3">
                                            <asp:Label ID="txt_DoiTuong" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Loại Sử Dụng
                                        </th>
                                        <td>
                                            <asp:Label ID="txt_Loai" runat="server"></asp:Label>
                                        </td>
                                        <th>
                                            Lượt Xem
                                        </th>
                                        <td>
                                            <asp:Label ID="txt_LuotXem" runat="server" ></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Chất Liệu
                                        </th>
                                        <td>
                                            <asp:Label ID="txt_ChatLieu" runat="server"></asp:Label>
                                        </td>
                                        <th>
                                            Lượt Thích
                                        </th>
                                        <td>
                                            <asp:Label ID="txt_LuotThich" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Năm Sản Xuất
                                        </th>
                                        <td>
                                            <asp:Label ID="txt_NamSX" runat="server"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-sm-3" style="padding-top: 7px">
                                    Giỏ Hàng Có
                                </div>
                                <div class="col-sm-4" style="padding-left:0px;padding-right:0px">
                                    <asp:TextBox ID="txt_GioHang" runat="server" CssClass="form-control" TextMode="Number" MaxLength="4" ></asp:TextBox>
                                </div>
                                <div class="col-sm-5">
                                    <asp:Button ID="btn_UpdateGio" runat="server" Text="Cập Nhật Giỏ Hàng" CssClass="btn btn-primary" Width="100%" OnClick="btn_UpdateGio_Click"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div style="width:100%;background-color:#5bc0de; margin: 15px auto auto auto; font-weight:bolder; font-size: 20px; padding: 5px 10px 5px 20px">
                    Mô Tả Chi Tiết
                </div>
                <div style="min-height:200px; padding: 10px 10px 10px 10px">
                    <asp:Literal ID="txt_MoTa" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-warning">
                <div class="panel panel-heading">
                    Có Thể Bạn Quan Tâm
                </div>
                <div class="panel panel-body">
                    <div>
                        <uc1:MatHangIcon runat="server" ID="txt_MHIcon1" />
                    </div>
                    <div>
                        <uc1:MatHangIcon runat="server" ID="txt_MHIcon2" />
                    </div>
                    <div>
                        <uc1:MatHangIcon runat="server" ID="txt_MHIcon3" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
