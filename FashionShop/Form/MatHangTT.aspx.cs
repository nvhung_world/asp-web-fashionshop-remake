﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FashionShop.SQL;
namespace FashionShop.Form
{
    public partial class MatHangTT : System.Web.UI.Page
    {
        string ID_MH = "",
            ID_ND = "",
            ID_DH = "",
            ID_DHCT = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            if (Session["User"] == null)
            {
                txt_GioHang.Visible = false;
                btn_UpdateGio.Text = "Đăng Nhập";
            }
            else
            {
                ID_ND = (Session["User"] as SqlUser).ID;
                txt_GioHang.Visible = true;
                btn_UpdateGio.Text = "Cập Nhật Giỏ Hàng";
            }

            if (Request.QueryString["IDMH"] != null)
                ID_MH = Request.QueryString["IDMH"];

            if (!IsPostBack)
            { 
                SqlAdapter adapter = new SqlAdapter();
                adapter.Connect();

                Load_MatHang_TT(adapter);

                adapter.Close();
            }
            Load_QuangCao();
        }
        private void Load_MatHang_TT(SqlAdapter adapter)
        {
            string query = string.Format("SELECT Top 1 TenMH, GiaBan, ThuongHieu, Loai, DoiTuong, ChatLieu, NamSX, LuotXem, LuotThich, MoTa FROM MatHang right join MatHang_TT on MatHang.ID_MH = MatHang_TT.ID_MHTT where DangBan = 1 and ID_MH = {0}",
                ID_MH);
            SqlResult res = adapter.ExecuteTable(query);
            if(res.isSuccess)
            {
                if(res.valueRow > 0)
                {
                    txt_TenMH.Text = res[0, 0].ToString();
                    txt_GiaBan.Text = res[0, 1].ToString();
                    txt_ThuongHieu.Text = res[0, 2].ToString();
                    txt_Loai.Text = res[0, 3].ToString();
                    txt_DoiTuong.Text = res[0, 4].ToString();
                    txt_ChatLieu.Text = res[0, 5].ToString();
                    txt_NamSX.Text = res[0, 6].ToString();
                    txt_LuotXem.Text = res[0, 7].ToString();
                    txt_LuotThich.Text = res[0, 8].ToString();
                    txt_MoTa.Text = res[0, 9].ToString();

                    Load_MH_Image(adapter);
                    TangLuotXem(adapter);
                    Load_GioHang(adapter);
                }
                else
                {
                    ID_MH = "";
                    btn_UpdateGio.Visible = txt_GioHang.Visible = false;
                }
            }
            else
            {
                ID_MH = "";
                btn_UpdateGio.Visible = txt_GioHang.Visible = false;
            }
        }
        private void Load_MH_Image(SqlAdapter adapter)
        {
            Image[] img = new Image[4];
            img[0] = txt_Image1;
            img[1] = txt_Image2;
            img[2] = txt_Image3;
            img[3] = txt_Image4;
            string query = string.Format("SELECT top 4 ID_MHA, Anh, Loai FROM MatHang_Anh where ID_MH = {0} order by loai asc",
                ID_MH);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                for (int i = 0; i < res.valueRow; i++)
                {
                    string ID_MHA = res[i, 0].ToString();
                    img[i].Attributes.Add("ID_MHA", ID_MHA);
                    string url = SqlUnit.convertStringToImageUrl(res[i, 1].ToString());
                    if (url != "")
                        img[i].ImageUrl = url;
                    else
                        img[i].ImageUrl = "~/Image/NoImage-icon.png";
                }
            }
        }
        private void TangLuotXem(SqlAdapter adapter)
        {
            string query = "UPDATE MatHang_TT SET LuotXem += 1 WHERE ID_MHTT = " + ID_MH;
            SqlResult res = adapter.ExecuteNonTable(query);
        }
        private void Load_QuangCao()
        {
            SqlAdapter adapter = new SqlAdapter();
            adapter.Connect();
            MatHangIcon[] MH_Icons = new MatHangIcon[] { txt_MHIcon1, txt_MHIcon2, txt_MHIcon3 };
            string query =string.Format("SELECT top 3 ID_MHTT, Anh, GiaBan, TenMH, TonKho, ThuongHieu FROM MatHang_TT inner join MatHang on MatHang_TT.ID_MHTT = MatHang.ID_MH left join MatHang_Anh on (MatHang.ID_MH = MatHang_Anh.ID_MH and MatHang_Anh.Loai = 0) where MatHang_TT.DangBan = 1  and MatHang.ID_MH != {0} ORDER BY GiaBan desc, MatHang_Anh.Loai asc",
                ID_MH == "" ? "-1" : ID_MH);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (i < res.valueRow)
                    {
                        MH_Icons[i].Visible = true;
                        MH_Icons[i].ID_MH = res[i, 0].ToString();
                        string imageUrl = SqlUnit.convertStringToImageUrl(res[i, 1].ToString());
                        if (imageUrl != "")
                            MH_Icons[i].ImageUrl = imageUrl;
                        else
                            MH_Icons[i].ImageUrl = "../Image/NoImage-icon.png";
                        MH_Icons[i].GiaBan = res[i, 2].ToString();
                        MH_Icons[i].TenMH = res[i, 3].ToString();
                        MH_Icons[i].isOK = res[i, 4].ToString() == "" ? false : true;
                        MH_Icons[i].ThuongHieu = res[i, 5].ToString();
                    }
                    else
                        MH_Icons[i].Visible = false;
                }
            }
            adapter.Close();
        }
        
        private void Load_GioHang(SqlAdapter adapter)
        {
            if (ID_MH == "" || ID_ND == "")
                return;

            string query = string.Format("select DonHang.ID_DH, ID_DHCT, SoLuong  from DonHang left join DonHang_CT on ( DonHang.ID_DH = DonHang_CT.ID_DH and ID_MH = {1}) where DonHang.TrangThai like N'Giỏ Hàng' and ID_ND = {0}",
                ID_ND,
                ID_MH);
            SqlResult res = adapter.ExecuteTable(query);
            if(res.isSuccess)
            {
                if (res.valueRow > 0)
                {
                    ID_DH = res[0, 0].ToString();
                    ID_DHCT = res[0, 1].ToString();
                    txt_GioHang.Text = res[0, 2].ToString() == "" ? "0" : res[0, 2].ToString();
                }
                else
                    txt_GioHang.Text = "0";
            }
        }
        protected void btn_UpdateGio_Click(object sender, EventArgs e)
        {
            if (ID_MH == "")
                return;

            if (ID_ND != "")
            {
                int soLuong = 0;
                int.TryParse(txt_GioHang.Text, out soLuong);
                if (soLuong < 0)
                    soLuong = 0;
                SqlAdapter adapter = new SqlAdapter();
                adapter.Connect();
                Load_GioHang(adapter);
                string error = "";
                if (ID_DH == "" && soLuong > 0)
                    error = Inset_DonHang(adapter, soLuong);
                else if (ID_DH != "")
                {
                    string query = "";
                    if (ID_DHCT == "" && soLuong > 0)
                        query = string.Format(" INSERT INTO DonHang_CT (ID_DH , ID_MH , SoLuong) VALUES ({0},{1},{2})",
                            ID_DH, 
                            ID_MH, 
                            soLuong);
                    else if(ID_DHCT != "" && soLuong == 0)
                        query = string.Format("DELETE FROM DonHang_CT  WHERE ID_DHCT = {0}",
                            ID_DHCT);
                    if (ID_DHCT != "" && soLuong > 0)
                        query = string.Format("UPDATE DonHang_CT SET SoLuong = {0} WHERE ID_DHCT = {1}",
                            soLuong, ID_DHCT);
                    if(query != "")
                    {
                        SqlResult res = adapter.ExecuteNonTable(query);
                        if (res.isSuccess)
                        {
                            if (res.valueRow < 1)
                                error = "Không có row bị tác động";
                        }
                        else
                            error = res.error;
                    }
                }

                if(error != "")
                    Response.Write("<script>alert('Lỗi: Thất bại khi cập nhật giỏ hàng');</script>");
                Load_GioHang(adapter);
                adapter.Close();
            }
            else
                Response.Redirect("TaiKhoan/Login.aspx");
        }
        private string Inset_DonHang(SqlAdapter adapter, int soLuong)
        {
            string query = string.Format("INSERT INTO DonHang (ID_ND) VALUES ({0}); INSERT INTO DonHang_CT (ID_DH ,ID_MH , SoLuong ) VALUES (IDENT_CURRENT('DonHang'), {1} , {2} );",
                ID_ND,
                ID_MH,
                soLuong);
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow < 1)
                    return "Không có row bị tác động";
                else
                    return "";
            }
            else
                return res.error;
        }
    }
}