﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
namespace FashionShop.SQL
{
    public class SqlAdapter
    {
        private string connectString = "Data Source=DESKTOP-HP1JMQT;Initial Catalog=FashionShop;Integrated Security=True";
        private SqlConnection conn;
        private bool is_Connect = false;
        public bool isConnect
        {
            get
            {
                return is_Connect;
            }
        }
        public SqlAdapter()
        {

        }
        public SqlAdapter(string connectString)
        {
            this.connectString = connectString;
        }
        ~SqlAdapter()
        {
            if (isConnect)
                Close();
        }
        public string Connect()
        {
            if (isConnect)
                return "Can't connect, Sql Connecting";
            string res = "";
            try
            {
                conn = new SqlConnection(connectString);
                conn.Open();
                is_Connect = true;
            }
            catch(Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        public string Close()
        {
            if (!isConnect)
                return "Can't close, Sql Closing";
            string res = "";
            try
            {
                conn.Close();
                is_Connect = false;
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
        
        public SqlResult ExecuteTable(string query)
        {
            if (!isConnect)
                return new SqlResult("SQl not conncet");
            SqlResult result = null;
            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(query, conn);
                adapter.Fill(dt);
                result = new SqlResult(dt);
            }
            catch(Exception ex)
            {
                result = new SqlResult(ex.Message);
            }
            return result;
        }
        public SqlResult ExecuteMultiTable(string query)
        {
            if (!isConnect)
                return new SqlResult("SQl not conncet");
            SqlResult result = null;
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter(query, conn);
                adapter.Fill(ds);
                result = new SqlResult(ds);
            }
            catch (Exception ex)
            {
                result = new SqlResult(ex.Message);
            }
            return result;
        }
        public SqlResult ExecuteNonTable(string query)
        {
            if (!isConnect)
                return new SqlResult("SQl not conncet");
            SqlResult result = null;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                int numberRow = cmd.ExecuteNonQuery();
                result = new SqlResult(numberRow);
            }
            catch (Exception ex)
            {
                result = new SqlResult(ex.Message);
            }
            return result;
        }
        public SqlCommand GetCommand(string query)
        {
            if (!isConnect)
                return null;
            else
                return new SqlCommand(query, conn);
        }
    }
}