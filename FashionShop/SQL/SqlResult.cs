﻿using System.Data;
namespace FashionShop.SQL
{
    public class SqlResult
    {
        public readonly bool isSuccess;
        public readonly string error;
        public DataTable table
        {
            get
            {
                if (dataset == null || dataset.Tables.Count <= 0)
                    return null;
                return dataset.Tables[0];
            }
        }
        public readonly DataSet dataset;
        public readonly int valueRow;
        public object Scalar
        {
            get
            {
                if (table == null || RowCount <= 0 || ColCount <= 0)
                    return null;
                return table.Rows[0][0];
            }
        }

        public int RowCount
        {
            get
            {
                if (table == null)
                    return -1;
                return table.Rows.Count;
            }
        }
        public int ColCount
        {
            get
            {
                if (table == null)
                    return -1;
                return table.Columns.Count;
            }
        }
        public object this[int x, int y]
        {
            get{
                if (table == null || x < 0 || x >= RowCount || y < 0 || y > ColCount)
                    return null;
                return table.Rows[x][y];
            }
        }

        public int TableCount
        {
            get
            {
                if (dataset == null)
                    return -1;
                return dataset.Tables.Count;
            }
        }
        private int[] tbRowCount, tbColCount;
        public int[] TbRowCount
        {
            get
            {
                return tbRowCount;
            }
        }
        public int[] TbColCount
        {
            get
            {
                return tbColCount;
            }
        }
        public DataTable this[int tb]
        {
            get
            {
                if (dataset == null || tb < 0 || tb >= TableCount)
                    return null;
                return dataset.Tables[tb];
            }
        }
        public object this[int tb, int x, int y]
        {
            get
            {
                if (dataset == null || tb < 0 || tb >= TableCount || x < 0 || x >= TbRowCount[tb] || y < 0 || y > TbColCount[tb])
                    return null;
                return dataset.Tables[tb].Rows[x][y];
            }
        }

        public SqlResult(string error)
        {
            isSuccess = false;
            this.error = error;
            dataset = null;
            valueRow = -1;
        }
        public SqlResult(DataTable table)
        {
            isSuccess = true;
            error = "";
            dataset = new DataSet();
            dataset.Tables.Add(table);
            valueRow = table.Rows.Count;
        }
        public SqlResult(DataSet dataset)
        {
            isSuccess = true;
            error = "";
            this.dataset = dataset;
            valueRow = 0;
            tbRowCount = new int[dataset.Tables.Count];
            tbColCount = new int[dataset.Tables.Count];
            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                valueRow += dataset.Tables[i].Rows.Count;
                tbRowCount[i] = dataset.Tables[i].Rows.Count;
                tbColCount[i] = dataset.Tables[i].Columns.Count;
            }
        }
        public SqlResult(int value)
        {
            isSuccess = true;
            error = "";
            dataset = null;
            this.valueRow = value;
        }
    }
}