﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace FashionShop.SQL
{
    public static class SqlUnit
    {
        public static string ConvertDateToSql(string var)
        {
            DateTime dt;
            if (var != null && DateTime.TryParse(var, out dt))
                return string.Format("{0}-{1}-{2}", dt.Year, dt.Month, dt.Day);
            else
                return "";
        }
        public static string ConvertSqlToDate(string var)
        {
            DateTime dt;
            if (var != null && DateTime.TryParse(var, out dt))
                return string.Format("{0}/{1}/{2}", dt.Day, dt.Month, dt.Year);
            else
                return "";
        }
        public static string ConvertImageToString(Stream fs)
        {
            string res = "";
            try
            {
                byte[] bytes;
                BinaryReader br = new BinaryReader(fs);
                bytes = br.ReadBytes((Int32)fs.Length);
                res = Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            catch (Exception)
            {
            }
            return res;
        }
        public static string convertStringToImageUrl(string Img)
        {
            string res = "";
            try
            {
                if (Img != null && Img != "")
                    res = "data:image/jpeg;base64," + Img;
            }
            catch (Exception)
            {
            }
            return res;
        }
        public static bool CheckExtensionImage(string Filename)
        {
            if (Filename.ToLower() == ".jpg" || 
                Filename.ToLower() == ".jpeg" || 
                Filename.ToLower() == ".png" ||
                Filename.ToLower() == ".gif" ||
                Filename.ToLower() == ".bmp")
                return true;
            else
                return false;
        }
    }
}