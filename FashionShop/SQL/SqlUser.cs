﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FashionShop.SQL
{
    public class SqlUser
    {
        private SqlAdapter adapter;
        private string id = "";
        private string Acc, Pass;
        private List<string> access;
        private Dictionary<string, string> info = null;
        private bool is_Block = false,
            is_Admin = false,
            is_Only_KhachHang = true;

        public string ID
        {
            get
            {
                return id;
            }
        }
        public List<string> Access
        {
            get
            {
                return access;
            }
        }
        public bool isBlock
        {
            get
            {
                return is_Block;
            }
        }
        public bool isAdmin
        {
            get
            {
                return is_Admin;
            }
        }
        public bool isOnlyKhachHang
        {
            get
            {
                return is_Only_KhachHang;
            }
        }

        public SqlUser(string Acc, string Pass)
        {
            this.Acc = Acc;
            this.Pass = Pass;
            adapter = new SqlAdapter();
            adapter.Connect();
            string query = string.Format("SELECT ID_ND FROM NguoiDung where LOWER(taikhoan) like N'{0}' and matkhau like N'{1}'",
                Acc, Pass);
            SqlResult res =  adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if(res.RowCount > 0)
                {
                    id = res[0, 0].ToString();
                    LoadAccess();
                    adapter.ExecuteNonTable("UPDATE NguoiDung SET DangNhapCuoi = GETDATE() WHERE id_ND = " + id);
                }
                else
                    throw new Exception("Sai tài khoản hoặc mật khẩu");
            }
            else
                throw new Exception("Không thể thực hiện đăng nhập");
        }
        public SqlUser(string ID)
        {
            adapter = new SqlAdapter();
            adapter.Connect();
            string query = string.Format("SELECT ID_ND, TaiKhoan, MatKhau FROM NguoiDung where ID_ND = {0}",
                ID);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess)
            {
                if (res.RowCount > 0)
                {
                    id = ID;
                    Acc = res[0, 1].ToString();
                    Acc = res[0, 2].ToString();
                    LoadAccess();
                    adapter.ExecuteNonTable("UPDATE NguoiDung SET DangNhapCuoi = GETDATE() WHERE id_ND = " + id);
                }
                else
                    throw new Exception("ID không tồn tại");
            }
            else
                throw new Exception("Không thể thực hiện");
        }
        public void LoadAccess()
        {
            string query = string.Format("select Noidung as 'Quyen' from PhanQuyen, Quyen where PhanQuyen.id_Q = Quyen.ID_Q and PhanQuyen.id_ND = {0}",
                ID);
            SqlResult res =  adapter.ExecuteTable(query);
            if(res.isSuccess && res.RowCount > 0)
            {
                access = new List<string>();
                for (int i = 0; i < res.RowCount; i++)
                {
                    access.Add(res[i, 0].ToString());
                    if (access[i] == "Đã Khóa")
                        is_Block = true;
                    else if (access[i] != "Khách Hàng")
                    {
                        is_Only_KhachHang = false;
                        if (access[i] == "Admin")
                            is_Admin = true;
                    }
                }
            }
            else
            {
                access = new List<string>(new string[]{ "Khách Hàng" });
            }
        }
        public string ChangerAccsess(bool isADD, string access)
        {
            string query = "";
            if(isADD)
            {
                if (Access.Contains(access) == false)
                    query = string.Format("INSERT INTO PhanQuyen (ID_ND ,ID_Q) VALUES ( {0}, (select ID_Q from Quyen where Quyen.NoiDung like N'{1}'))",
                        ID,
                        access);
                else
                    return "";
            }
            else
            {
                if (Access.Contains(access) == true)
                    query = string.Format("DELETE FROM PhanQuyen WHERE ID_ND = {0} and PhanQuyen.ID_Q = (select Quyen = ID_Q from Quyen where Quyen.NoiDung like N'{1}');",
                        ID,
                        access);
                else
                    return "";
            }
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow < 1)
                    return "Không có trường được thực hiện";
                else if (isADD)
                    Access.Add(access);
                else
                    Access.Remove(access);
                return "";
            }
            else
                return res.error;
        }

        public string LoadInfo()
        {
            string err = "";
            string query = string.Format("select Anh, DangNhapCuoi, NgayLap, Ho, Ten, case when gioitinh = 0 then N'Nam' when gioitinh = 1 then N'Nữ' when gioitinh is null then N'Trống' end as 'GioiTinh', NgaySinh, CMT, SDT, Email, DiaChi from NguoiDung, NguoiDung_TT where NguoiDung.id_nd = NguoiDung_TT.id_ndtt and ID_ND = {0}",
                id);
            SqlResult res = adapter.ExecuteTable(query);
            if (res.isSuccess && res.RowCount > 0)
            {
                if (info == null)
                    info = new Dictionary<string, string>();
                else
                    info.Clear();
                for (int i=0; i< res.ColCount; i ++)
                {
                    info.Add(res.table.Columns[i].ColumnName, res[0, i].ToString());
                }
            }
            else
                err = res.error;
            return err;
        }
        public string UpdateInfo(string Anh, string Ten, string Ho, string GioiTinh, string NgaySinh, string CMT, string SDT, string Email, string diaChi)
        {
            string anhTmp = "";
            if (Anh != null && Anh != "")
                anhTmp = "Anh='" + Anh + "',";
            string query = string.Format("UPDATE NguoiDung_TT SET {0} Ten = N'{1}' ,Ho = N'{2}' ,GioiTinh = {3}, NgaySinh = '{4}', CMT = '{5}', SDT = '{6}', Email = N'{7}', DiaChi = N'{8}' WHERE ID_NDTT = {9}",
                anhTmp, Ten, Ho, GioiTinh, NgaySinh, CMT, SDT, Email, diaChi, ID);
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow > 0)
                    return "";
                else
                    return "Không trường nào bị tác động";
            }
            else
                return res.error;
        }
        public string GetInfo(string col, string Default = null)
        {
            if (info == null)
                return Default;
            string res = Default;
            info.TryGetValue(col, out res);
            return res;
        }
        public string ChangerPass(string oldPass, string newPass)
        {
            if (oldPass != Pass)
                return "Xác nhận mật khẩu cũ sai";
            if(newPass == null || newPass.Length < 4 || newPass.Length > 30)
                return "Mật khẩu mới phải trên 4 ký tự và dưới 30 ký tự";
            string query = string.Format("UPDATE NguoiDung SET MatKhau = N'{0}' WHERE ID_ND = {1}",
                newPass,
                ID);
            SqlResult res = adapter.ExecuteNonTable(query);
            if (res.isSuccess)
            {
                if (res.valueRow < 1)
                    return "Không tìm thấy tài khoản";
                else
                {
                    Pass = newPass;
                    return "Thành Công";
                }
            }
            else
                return "Không thể đổi mật khẩu";
        }
    }
}